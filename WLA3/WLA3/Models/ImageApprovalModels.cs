﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WLA3.Models
{
	public class ImageApprovalModels
	{
		public List<ImageInformation> ImageList { get; set; }
	}

	public class ImageInformation
	{
		public string ImageId { get; set; }
		public Int16 ImageSequence { get; set; }
		public string ImageResolution { get; set; }
		public string ImageType { get; set; }
		public string ImageData { get; set; }
		public string UserId { get; set; }
		public string Location { get; set; }
		public DateTime Stamp { get; set; }
		public DateTime? Uploaded { get; set; }
		public DateTime? DeleteDate { get; set; }
		public bool Approved { get; set; }
		public bool ImageChanged { get; set; }
		public bool ImageNew { get; set; }
		public bool FormChanged { get; set; }
		public Person PersonInfo { get; set; }
		public BreedRequest BreedRequestInfo { get; set; }
		public UtilityModel utilityModel { get; set; }
	}

	public class Person
	{
		[Display(Name = "First Name")]
		public string FirstName { get; set; }
		[Display(Name = "Last Name")]
		public string LastName { get; set; }
		[Display(Name = "Phone Number")]
		public string AreaCode { get; set; }
		public string PhoneNumber { get; set; }
		[Display(Name = "Lost/Found Address & Cross streets")]
		public string LostFoundAddress { get; set; }
		[Display(Name = "Patron Home Address")]
		public string PatronAddress { get; set; }
		[Display(Name = "City")]
		public string City { get; set; }
		[Display(Name = "State")]
		public string State { get; set; }
		[Display(Name = "Zip Code")]
		public string ZipCode { get; set; }
		[Display(Name = "Email Address")]
		public string EmailAddress { get; set; }
		[Display(Name = "Email Format")]
		public string EmailFormat { get; set; }
	}

	public class BreedRequest
	{
		public string BreedRequestId { get; set; }
		public string EditHash { get; set; }
		public string RequestStatus { get; set; }
		public string ShelterList { get; set; }
		public string BreedName { get; set; }
		public string ProperName { get; set; }
		public string InterestType { get; set; }
		public string AnimalType { get; set; }
		public string AnimalGender { get; set; }
		public string BreedGroup { get; set; }
		public string Age { get; set; }
		public string Size { get; set; }
		public string SimilarAnimals { get; set; }
		public string Color { get; set; }
		public string LocationLostFound { get; set; }
		public string AnimalDescription { get; set; }
		public string AllowPitBull { get; set; }
		public string EventDate { get; set; }
		public string PetName { get; set; }
		public string LostPetOwnerAddress { get; set; }
		public string Coat { get; set; }
		public string Tail { get; set; }
		public string Ears { get; set; }
		public string Nose { get; set; }
		public string CollarType { get; set; }
		public string CollarColor { get; set; }
		public string Tattoo { get; set; }
		public string SpecialEvent { get; set; }
		public string Microchip { get; set; }
	}


}