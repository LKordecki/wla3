﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;
using WLA3.Models;
using WLA3.ViewModels;

namespace WLA3.Controllers
{
	public class ImageApprovalController : Controller
	{

		DatabaseController dbC = new DatabaseController();
		UtilityController uc = new UtilityController();


		public ActionResult GetPendingImages()
		{
			var master = new Master_ViewModel.MVM();
			master.CLM = new CustomModels.ClientLookupModel();
			master.CLM.User = Session["User"].ToString();
			master.CLM.Password = Session["Password"].ToString();
			master.CLM.ClientId = "HLP";

			if (master.CLM == null || (master.CLM.User == null || master.CLM.Password == null || master.CLM.ClientId == null))
			{
				if (master.CLM == null)
					master.CLM = new CustomModels.ClientLookupModel();
				var unpw = dbC.GetUNPW();
				master.CLM.User = unpw.User;
				master.CLM.Password = unpw.Password;
				master.CLM.ClientId = unpw.ClientId;
			}

			dbC.SetUNPW("Animal", master.CLM.User, master.CLM.Password, master.CLM.ClientId);

			master.ImageApprovalMaster = new ImageApproval_ViewModel();
			master.ImageApprovalMaster.ImageList = new List<ImageInformation>(dbC.getPendingImages());

			foreach(var item in master.ImageApprovalMaster.ImageList)
      {
				var breedReqId = item.BreedRequestInfo.BreedRequestId;
				var patronEmail = item.PersonInfo.EmailAddress;
				item.BreedRequestInfo.EditHash = GetMD5Hash(breedReqId, patronEmail);
			}

			var DUM = new UtilityModel
			{
				animalType = uc.GetAnimalType(),
				stateList = uc.GetStates(),
				sexList = uc.GetAnimalSex(),
				requestFormat = uc.GetRequestFormat(),
				ageList = uc.GetAgeList(),
				breedInfo = uc.getBreedInfo(),
				requestStatus = uc.GetStatusList(),
				sizeList = uc.GetSizeList(),
				requestType = uc.GetInterestTypeList(),
				colorList = uc.GetColorList(),
				coatList = uc.GetCoatList(),
				tailList = uc.GetTailList(),
				earList = uc.GetEarList(),
				noseList = uc.GetNoseList(),
				collarType = uc.GetCollarTypeList(),
				collarColor = uc.GetCollarColorList()
			};
			master.ImageApprovalMaster.DUM = DUM;

			master.ImageApprovalMaster.TotalAnimalCount = dbC.getImageCount();

			return View(master);
		}

		[HttpPost]
		public ActionResult MassApproveImages(Master_ViewModel.MVM mVM) {
			ApproveItems(mVM.ImageApprovalMaster.ImageList);
			//dbC.approveAllImages(mVM.ImageApprovalMaster.ImageList);
			//dbC.updateBreedRequestInformation(mVM.ImageApprovalMaster.ImageList.Where(update => update.FormChanged == true).ToList());
			return GetDisapprovedImages(mVM);
		}

		[HttpPost]
		public ActionResult GetSpecificRequest(string ImageLookup)
		{
			var master = new Master_ViewModel.MVM();

			master.ImageApprovalMaster = new ImageApproval_ViewModel();
			master.ImageApprovalMaster.ImageList = dbC.lookupBy(ImageLookup);
			if (!String.IsNullOrWhiteSpace(ImageLookup))
			{
				master.ImageApprovalMaster.ImageList = dbC.lookupBy(ImageLookup);
			}

			foreach (var item in master.ImageApprovalMaster.ImageList)
			{
				var breedReqId = item.BreedRequestInfo.BreedRequestId;
				var patronEmail = item.PersonInfo.EmailAddress;
				item.BreedRequestInfo.EditHash = GetMD5Hash(breedReqId, patronEmail);
			}

			var DUM = new UtilityModel
			{
				animalType = uc.GetAnimalType(),
				stateList = uc.GetStates(),
				sexList = uc.GetAnimalSex(),
				requestFormat = uc.GetRequestFormat(),
				ageList = uc.GetAgeList(),
				breedInfo = uc.getBreedInfo(),
				requestStatus = uc.GetStatusList(),
				sizeList = uc.GetSizeList(),
				requestType = uc.GetInterestTypeList(),
				colorList = uc.GetColorList(),
				coatList = uc.GetCoatList(),
				tailList = uc.GetTailList(),
				earList = uc.GetEarList(),
				noseList = uc.GetNoseList(),
				collarType = uc.GetCollarTypeList(),
				collarColor = uc.GetCollarColorList()
			};
			master.ImageApprovalMaster.DUM = DUM;

			master.ImageApprovalMaster.TotalAnimalCount = 1;

			return View("GetSpecificRequest", master);
		}

		[HttpGet]
		public ActionResult GetDisapprovedImages(Master_ViewModel.MVM mVM)
		{
			var disapprovedImages = new List<ImageInformation>();

			foreach(var request in mVM.ImageApprovalMaster.ImageList)
			{
				if(request.Approved == false)
				{
					disapprovedImages.Add(dbC.getIndividualImageByID(request.ImageId, request.ImageSequence));
				}
			}

			mVM.ImageApprovalMaster.ImageList = disapprovedImages;

			return View("GetDisapprovedImages", mVM);
		}


		[HttpPost]
		public ActionResult SaveIndividualItem([Bind(Prefix = "item")] ImageInformation imageContents)
		{
			if(imageContents.ImageNew == true)
			{
				imageContents.ImageSequence += 1;
			}

			var imageList = new List<ImageInformation>();
			imageList.Add(imageContents);
			ApproveItems(imageList);

			return RedirectToAction("GetPendingImages");
		}


		internal void ApproveItems(List<ImageInformation> imageList)
		{
			dbC.approveAllImages(imageList);
			dbC.updateBreedRequestInformation(imageList.Where(update => update.FormChanged == true).ToList());
		}

		internal string GetMD5Hash(string breedReqId, string emailAddress)
		{
			var hashString = breedReqId + emailAddress;
			var verificationHash = MD5(hashString);
			return verificationHash.Substring(3, 8);
		}

		internal string MD5(string source)
		{
			using (var md5Service = new MD5CryptoServiceProvider())
			{
				var bytes = md5Service.ComputeHash(Encoding.ASCII.GetBytes(source));
				var hash = "";
				var builder = new StringBuilder();
				builder.Append(hash);
				foreach (byte ByteVal in bytes)
				{
					builder.Append(ByteVal.ToString("x2"));
				}
				hash = builder.ToString();
				return hash;
			}
		}
	}
}