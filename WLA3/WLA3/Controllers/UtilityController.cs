﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WLA3.Models;

namespace WLA3.Controllers
{
	public class UtilityController : Controller
	{
		DatabaseController dbC = new DatabaseController();

		internal IEnumerable<UtilityModel.AnimalType> GetAnimalType()
		{
			var animalTypesList = new List<UtilityModel.AnimalType>();

			animalTypesList.Add(new UtilityModel.AnimalType
			{
				animalTypeKey = "",
				animalTypeText = "--- Please Select ---"
			});
			animalTypesList.Add(new UtilityModel.AnimalType
			{
				animalTypeKey = "dog",
				animalTypeText = "Dog"
			});
			animalTypesList.Add(new UtilityModel.AnimalType
			{
				animalTypeKey = "cat",
				animalTypeText = "Cat"
			});
			animalTypesList.Add(new UtilityModel.AnimalType
			{
				animalTypeKey = "other",
				animalTypeText = "Other"
			});

			return animalTypesList;
		}

		internal IEnumerable<UtilityModel.DropdownList> GetAnimalSex()
		{
			var list = new List<UtilityModel.DropdownList>();
			list.Add(new UtilityModel.DropdownList
			{
				ddKey = "",
				ddText = "None Selected"
			});
			list.Add(new UtilityModel.DropdownList
			{
				ddKey = "female",
				ddText = "Female"
			});
			list.Add(new UtilityModel.DropdownList
			{
				ddKey = "male",
				ddText = "Male"
			});
			return list;
		}

		internal IEnumerable<UtilityModel.DropdownList> GetColorList()
		{
			var list = new List<UtilityModel.DropdownList>();
			list.Add(new UtilityModel.DropdownList
			{
				ddKey = "",
				ddText = "None Selected"
			});
			list.Add(new UtilityModel.DropdownList
			{
				ddKey = "white",
				ddText = "White"
			});
			list.Add(new UtilityModel.DropdownList
			{
				ddKey = "black",
				ddText = "Black"
			});
			list.Add(new UtilityModel.DropdownList
			{
				ddKey = "brown",
				ddText = "Brown"
			});
			list.Add(new UtilityModel.DropdownList
			{
				ddKey = "other",
				ddText = "Other"
			});
			return list;
		}

		internal IEnumerable<UtilityModel.DropdownList> GetCoatList()
		{
			var list = new List<UtilityModel.DropdownList>();
			list.Add(new UtilityModel.DropdownList
			{
				ddKey = "",
				ddText = "None Selected"
			});
			list.Add(new UtilityModel.DropdownList
			{
				ddKey = "curly",
				ddText = "Curly"
			});
			list.Add(new UtilityModel.DropdownList
			{
				ddKey = "long silky",
				ddText = "Long & Silky"
			});
			list.Add(new UtilityModel.DropdownList
			{
				ddKey = "long thck",
				ddText = "Long & Thick"
			});
			list.Add(new UtilityModel.DropdownList
			{
				ddKey = "medium",
				ddText = "Medium"
			});
			list.Add(new UtilityModel.DropdownList
			{
				ddKey = "short flat",
				ddText = "Short & Flat"
			});
			list.Add(new UtilityModel.DropdownList
			{
				ddKey = "short thck",
				ddText = "Short & Thick"
			});
			list.Add(new UtilityModel.DropdownList
			{
				ddKey = "wire",
				ddText = "Wire"
			});
			return list;
		}

		internal IEnumerable<UtilityModel.DropdownList> GetTailList()
		{
			var list = new List<UtilityModel.DropdownList>();
			list.Add(new UtilityModel.DropdownList
			{
				ddKey = "",
				ddText = "None Selected"
			});
			list.Add(new UtilityModel.DropdownList
			{
				ddKey = "bobtail",
				ddText = "Bobtail"
			});
			list.Add(new UtilityModel.DropdownList
			{
				ddKey = "curly bshy",
				ddText = "Curly & Bushey"
			});
			list.Add(new UtilityModel.DropdownList
			{
				ddKey = "curly smth",
				ddText = "Curly & Smooth"
			});
			list.Add(new UtilityModel.DropdownList
			{
				ddKey = "long bshy",
				ddText = "Long & Bushey"
			});
			list.Add(new UtilityModel.DropdownList
			{
				ddKey = "long smth",
				ddText = "Long & Smooth"
			});
			list.Add(new UtilityModel.DropdownList
			{
				ddKey = "long wire",
				ddText = "Long Wire"
			});
			list.Add(new UtilityModel.DropdownList
			{
				ddKey = "short smth",
				ddText = "Short & Smooth"
			});
			return list;
		}

		internal IEnumerable<UtilityModel.DropdownList> GetEarList()
		{
			var list = new List<UtilityModel.DropdownList>();
			list.Add(new UtilityModel.DropdownList
			{
				ddKey = "",
				ddText = "None Selected"
			});
			list.Add(new UtilityModel.DropdownList
			{
				ddKey = "cropped",
				ddText = "Cropped"
			});
			list.Add(new UtilityModel.DropdownList
			{
				ddKey = "droopy",
				ddText = "Droopy"
			});
			list.Add(new UtilityModel.DropdownList
			{
				ddKey = "drop",
				ddText = "Drop"
			});
			list.Add(new UtilityModel.DropdownList
			{
				ddKey = "erect",
				ddText = "Erect"
			});
			list.Add(new UtilityModel.DropdownList
			{
				ddKey = "rose",
				ddText = "Rose"
			});
			list.Add(new UtilityModel.DropdownList
			{
				ddKey = "semi erect",
				ddText = "Semi-erect"
			});
			return list;
		}

		internal IEnumerable<UtilityModel.DropdownList> GetCollarTypeList()
		{
			var list = new List<UtilityModel.DropdownList>();
			list.Add(new UtilityModel.DropdownList
			{
				ddKey = "",
				ddText = "None Selected"
			});
			list.Add(new UtilityModel.DropdownList
			{
				ddKey = "none",
				ddText = "None"
			});
			list.Add(new UtilityModel.DropdownList
			{
				ddKey = "canvas",
				ddText = "Canvas"
			});
			list.Add(new UtilityModel.DropdownList
			{
				ddKey = "choke chn",
				ddText = "Choke Chain"
			});
			list.Add(new UtilityModel.DropdownList
			{
				ddKey = "flea",
				ddText = "Flea"
			});
			list.Add(new UtilityModel.DropdownList
			{
				ddKey = "harness",
				ddText = "Harness"
			});
			list.Add(new UtilityModel.DropdownList
			{
				ddKey = "leather",
				ddText = "Leather"
			});
			list.Add(new UtilityModel.DropdownList
			{
				ddKey = "nylon",
				ddText = "Nylon"
			});
			list.Add(new UtilityModel.DropdownList
			{
				ddKey = "plastic",
				ddText = "Plastic"
			});
			list.Add(new UtilityModel.DropdownList
			{
				ddKey = "rope",
				ddText = "Rope"
			});
			list.Add(new UtilityModel.DropdownList
			{
				ddKey = "spiked",
				ddText = "Spiked"
			});
			list.Add(new UtilityModel.DropdownList
			{
				ddKey = "studded",
				ddText = "Studded"
			});
			list.Add(new UtilityModel.DropdownList
			{
				ddKey = "other",
				ddText = "Other"
			});
			return list;
		}

		internal IEnumerable<UtilityModel.DropdownList> GetCollarColorList()
		{
			var list = new List<UtilityModel.DropdownList>();
			list.Add(new UtilityModel.DropdownList
			{
				ddKey = "",
				ddText = "None Selected"
			});
			list.Add(new UtilityModel.DropdownList
			{
				ddKey = "black",
				ddText = "Black"
			});
			list.Add(new UtilityModel.DropdownList
			{
				ddKey = "blue",
				ddText = "Blue"
			});
			list.Add(new UtilityModel.DropdownList
			{
				ddKey = "brown",
				ddText = "Brown"
			});
			list.Add(new UtilityModel.DropdownList
			{
				ddKey = "green",
				ddText = "Green"
			});
			list.Add(new UtilityModel.DropdownList
			{
				ddKey = "multi",
				ddText = "Multicolored"
			});
			list.Add(new UtilityModel.DropdownList
			{
				ddKey = "orange",
				ddText = "Orange"
			});
			list.Add(new UtilityModel.DropdownList
			{
				ddKey = "pink",
				ddText = "Pink"
			});
			list.Add(new UtilityModel.DropdownList
			{
				ddKey = "purple",
				ddText = "Purple"
			});
			list.Add(new UtilityModel.DropdownList
			{
				ddKey = "red",
				ddText = "Red"
			});
			list.Add(new UtilityModel.DropdownList
			{
				ddKey = "silver",
				ddText = "Silver"
			});
			list.Add(new UtilityModel.DropdownList
			{
				ddKey = "tan",
				ddText = "Tan"
			});
			list.Add(new UtilityModel.DropdownList
			{
				ddKey = "white",
				ddText = "White"
			});
			list.Add(new UtilityModel.DropdownList
			{
				ddKey = "yellow",
				ddText = "Yellow"
			});
			return list;
		}

		internal IEnumerable<UtilityModel.DropdownList> GetNoseList()
		{
			var list = new List<UtilityModel.DropdownList>();
			list.Add(new UtilityModel.DropdownList
			{
				ddKey = "",
				ddText = "None Selected"
			});
			list.Add(new UtilityModel.DropdownList
			{
				ddKey = "brachy",
				ddText = "Flat (ex. Pug)"
			});
			list.Add(new UtilityModel.DropdownList
			{
				ddKey = "long",
				ddText = "Long"
			});
			list.Add(new UtilityModel.DropdownList
			{
				ddKey = "long beard",
				ddText = "Long w/ Beard"
			});
			list.Add(new UtilityModel.DropdownList
			{
				ddKey = "medium",
				ddText = "Medium"
			});
			list.Add(new UtilityModel.DropdownList
			{
				ddKey = "short",
				ddText = "Short"
			});
			return list;
		}


		internal IEnumerable<UtilityModel.RequestFormat> GetRequestFormat()
		{
			var list = new List<UtilityModel.RequestFormat>();
			list.Add(new UtilityModel.RequestFormat
			{
				formatKey = "HTML",
				formatText = "HTML"
			});
			list.Add(new UtilityModel.RequestFormat
			{
				formatKey = "PlainText",
				formatText = "PlainText"
			});
			return list;
		}

		internal IEnumerable<UtilityModel.DropdownList> GetInterestTypeList()
		{
			var list = new List<UtilityModel.DropdownList>();
			list.Add(new UtilityModel.DropdownList
			{
				ddKey = "Animal Reclaim",
				ddText = "Animal Reclaim"
			});
			list.Add(new UtilityModel.DropdownList
			{
				ddKey = "Potential Adoption",
				ddText = "Potential Adoption"
			});
			list.Add(new UtilityModel.DropdownList
			{
				ddKey = "Return to Owner",
				ddText = "Return to Owner"
			});
			return list;
		}

		internal IEnumerable<UtilityModel.DropdownList> GetAgeList()
		{
			var list = new List<UtilityModel.DropdownList>();
			list.Add(new UtilityModel.DropdownList
			{
				ddKey = " ",
				ddText = "None Selected"
			});
			list.Add(new UtilityModel.DropdownList
			{
				ddKey = "younger",
				ddText = "Young: < 1 Year"
			});
			list.Add(new UtilityModel.DropdownList
			{
				ddKey = "older",
				ddText = "Older: > 1 Year"
			});
			return list;
		}


		internal IEnumerable<UtilityModel.DropdownList> GetStatusList()
		{
			var list = new List<UtilityModel.DropdownList>();
			list.Add(new UtilityModel.DropdownList
			{
				ddKey = "ACTIVE",
				ddText = "Active"
			});
			list.Add(new UtilityModel.DropdownList
			{
				ddKey = "EXPIRED",
				ddText = "Expired"
			});
			list.Add(new UtilityModel.DropdownList
			{
				ddKey = "EXP PEND",
				ddText = "Exp Pending"
			});
			list.Add(new UtilityModel.DropdownList
			{
				ddKey = "CANCELLED",
				ddText = "Cancelled"
			});
			list.Add(new UtilityModel.DropdownList
			{
				ddKey = "PENDING",
				ddText = "Pending"
			});
			list.Add(new UtilityModel.DropdownList
			{
				ddKey = "ERR_EMLERR",
				ddText = "Email Error"
			});
			return list;
		}

		internal IEnumerable<UtilityModel.DropdownList> GetSizeList()
		{
			var list = new List<UtilityModel.DropdownList>();
			list.Add(new UtilityModel.DropdownList
			{
				ddKey = "",
				ddText = "None Selected"
			});
			list.Add(new UtilityModel.DropdownList
			{
				ddKey = "toy",
				ddText = "Toy"
			});
			list.Add(new UtilityModel.DropdownList
			{
				ddKey = "small",
				ddText = "Small"
			});
			list.Add(new UtilityModel.DropdownList
			{
				ddKey = "med",
				ddText = "Medium"
			});
			list.Add(new UtilityModel.DropdownList
			{
				ddKey = "large",
				ddText = "Large"
			});
			list.Add(new UtilityModel.DropdownList
			{
				ddKey = "x-lrg",
				ddText = "Extra Large"
			});
			return list;
		}


		internal IEnumerable<UtilityModel.StateList> GetStates() => new List<UtilityModel.StateList>(dbC.getStatesList());

		internal IEnumerable<UtilityModel.BreedInfo> getBreedInfo() => new List<UtilityModel.BreedInfo>(dbC.getBreedInfo());

	}
}