﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WLA3.Models;
using WLA3.ViewModels;
using static WLA3.Models.CustomModels;


namespace WLA3.Controllers
{
	public class WLAController : Controller
	{

		DatabaseController dbC = new DatabaseController();

		public ActionResult Index() => View(new Master_ViewModel.MVM());


		/// <summary>
		/// Takes in the shelter code and SQL connection info to return all the jurisdictions for that shelter - First page after hitting 'WLA3' Selection in header
		/// </summary>
		/// <param name="wlavm"></param>
		/// <returns></returns>
		//[Route("GetIdentifierList")]
		[HttpGet]
		public ActionResult GetIdentifierList(/*ClientLookupModel clientInfo*/ /*WLA_ViewModel.WLAVM wlavm*/)
		{
			var master = new Master_ViewModel.MVM();
			master.WLAVMMaster = new WLA_ViewModel.WLAVM();
			master.WLAVMMaster.ClientLookupModel = new ClientLookupModel();
			master.WLAVMMaster.ClientLookupModel.User = Session["User"].ToString();
			master.WLAVMMaster.ClientLookupModel.Password = Session["Password"].ToString();
			master.WLAVMMaster.ClientLookupModel.ClientId = Session["ClientId"].ToString();

			if (master.WLAVMMaster.ClientLookupModel == null || (master.WLAVMMaster.ClientLookupModel.User == null || master.WLAVMMaster.ClientLookupModel.Password == null || master.WLAVMMaster.ClientLookupModel.ClientId == null))
			{
				if (master.WLAVMMaster == null)
					master.WLAVMMaster = new WLA_ViewModel.WLAVM();
				if (master.WLAVMMaster.ClientLookupModel == null)
					master.WLAVMMaster.ClientLookupModel = new ClientLookupModel();
				var unpw = dbC.GetUNPW();
				master.WLAVMMaster.ClientLookupModel.User = unpw.User;
				master.WLAVMMaster.ClientLookupModel.Password = unpw.Password;
				master.WLAVMMaster.ClientLookupModel.ClientId = unpw.ClientId;
			}

			dbC.SetUNPW("WA", master.WLAVMMaster.ClientLookupModel.User, master.WLAVMMaster.ClientLookupModel.Password, master.WLAVMMaster.ClientLookupModel.ClientId);
			//var master.WLAVMMaster = new WLA_ViewModel.WLAVM();
			master.WLAVMMaster.CM_ILL = new List<IdentifierLookupList>();
			master.WLAVMMaster.CM_ILL.Add(new IdentifierLookupList
			{
				JurisdictionId = 0,
				ClientId = master.WLAVMMaster.ClientLookupModel.ClientId.ToUpper(),
				JurisdictionName = "Default",
				JurisdictionZipcodes = "All"
			});

			var lists = dbC.getIdentifierList(master.WLAVMMaster);

			foreach (var item in lists)
			{
				item.JurisdictionZipcodes = item.JurisdictionZipcodes.Replace(",", ", ").Replace('.', ',').Replace(" ,  ", ", ");
				master.WLAVMMaster.CM_ILL.Add(item);
			}

			master.WLAVMMaster.JInfo = new List<JurisdictionConfiguration>();
			master.WLAVMMaster.JInfo = dbC.getJurisdictionInfo(master.WLAVMMaster.ClientLookupModel.ClientId);

			master.WLAVMMaster.LBInfo = new List<LockboxConfiguration>();
			master.WLAVMMaster.LBInfo = dbC.getLockboxValidations(master.WLAVMMaster.ClientLookupModel.ClientId.ToUpper());

			return View("ClientPreferences", master);
		}

		/// <summary>
		/// Allows the adding of template/non-template Jurisdiction and Price records
		/// </summary>
		/// <param name="identifierListParams"></param>
		/// <returns></returns>
		[HttpGet]
		public ActionResult AddJurisdiction(IdentifierListParams identifierListParams)
		{
			var master = new Master_ViewModel.MVM();
			master.WLAVMMaster = new WLA_ViewModel.WLAVM();
			master.WLAVMMaster.ClientLookupModel = new ClientLookupModel();
			master.WLAVMMaster.ClientLookupModel.ClientId = identifierListParams.ClientId;


			if (identifierListParams.ActionType == "Temp")
			{
				//find our template to fill out our new form
				var templateJurisdiction = dbC.GetTemplateJurisdiction(identifierListParams.ClientId, identifierListParams.JurisdictionId);
				master.WLAVMMaster.JConfig = templateJurisdiction;

				var templateJPriceRecords = dbC.GetJurisdictionPriceRecords(identifierListParams.ClientId, identifierListParams.JurisdictionId);
				///Add new template price records to get up to a max of 40 so we can add or remove records
				for (var i = templateJPriceRecords.Count; i < 40; i++)
				{
					templateJPriceRecords.Add(new JurisdictionPriceConfiguration()
					{
						InUse = false
					});
				}

				master.WLAVMMaster.JConfig.JurisdictionPrice = new List<JurisdictionPriceConfiguration>();
				master.WLAVMMaster.JConfig.JurisdictionPrice = templateJPriceRecords;
			}
			else
			{
				master.WLAVMMaster.JConfig = new JurisdictionConfiguration();
				master.WLAVMMaster.JConfig.ClientId = identifierListParams.ClientId;
				master.WLAVMMaster.JConfig.JurisdictionId = identifierListParams.JurisdictionId;

				master.WLAVMMaster.JConfig.JurisdictionPrice = new List<JurisdictionPriceConfiguration>();
				///Add new template price records to get up to a max of 40 so we can add or remove records
				for (var i = 0; i < 40; i++)
				{
					master.WLAVMMaster.JConfig.JurisdictionPrice.Add(new JurisdictionPriceConfiguration()
					{
						InUse = false
					});
				}
			}


			return View("AddJurisdiction", master);
		}

		/// <summary>
		/// Takes new Jurisdiction and Price information and adds them to the database
		/// </summary>
		/// <param name="wlavm"></param>
		/// <returns></returns>
		[HttpPost]
    [ValidateInput(false)]
    public ActionResult AddJurisdiction(Master_ViewModel.MVM master)
		{
			if (master.WLAVMMaster.JConfig != null)
			{
				var jurisdictionId = dbC.InsertJurisdiction(master.WLAVMMaster.JConfig);
				if (master.WLAVMMaster.JConfig.JurisdictionPrice != null)
				{
					foreach (var item in master.WLAVMMaster.JConfig.JurisdictionPrice)
					{
						if (item.InUse == true)
						{
							dbC.InsertJurisdictionPrice(item, jurisdictionId);
						}
					}
				}
			}

			return RedirectToAction("GetIdentifierList");
		}




		[HttpGet]
		public ActionResult EditJurisdictionPrice(int jurisdictionId)
		{
			var master = new Master_ViewModel.MVM();
			master.WLAVMMaster = new WLA_ViewModel.WLAVM();
			var clientId = Session["ClientId"].ToString();
			//master.WLAVMMaster.ClientLookupModel = new ClientLookupModel();
			//master.WLAVMMaster.ClientLookupModel.User = Session["User"].ToString();
			//master.WLAVMMaster.ClientLookupModel.Password = Session["Password"].ToString();
			//master.WLAVMMaster.ClientLookupModel.ClientId = Session["ClientId"].ToString();
			//// Get Jurisdiction records
			//// Display all records for that jurisdiction
			master.WLAVMMaster.JConfig = new JurisdictionConfiguration();
			master.WLAVMMaster.JConfig.ClientId = Session["ClientId"].ToString();
			master.WLAVMMaster.JConfig.JurisdictionId = jurisdictionId;

			var templateJurisdiction = dbC.GetTemplateJurisdiction(clientId, jurisdictionId);
			master.WLAVMMaster.JConfig = templateJurisdiction;

			var templateJPriceRecords = dbC.GetJurisdictionPriceRecords(clientId, jurisdictionId);
			///Add new template price records to get up to a max of 40 so we can add or remove records
			for (var i = templateJPriceRecords.Count; i < 40; i++)
			{
				templateJPriceRecords.Add(new JurisdictionPriceConfiguration()
				{
					ClientId = Session["ClientId"].ToString(),
					JurisdictionId = jurisdictionId,
					IsUpdate = false,
					InUse = false
				});
			}

			master.WLAVMMaster.JConfig.JurisdictionPrice = new List<JurisdictionPriceConfiguration>();
			master.WLAVMMaster.JConfig.JurisdictionPrice = templateJPriceRecords;


			return View("UpdateJurisdictionPrice", master);
		}

		[HttpPost]
    [ValidateInput(false)]
    public ActionResult EditJurisdictionPrice(Master_ViewModel.MVM master)
		{

			foreach (var item in master.WLAVMMaster.JConfig.JurisdictionPrice)
			{
				// Create
				if (item.InUse == true && item.JurisdictionPriceId == 0)
				{
					dbC.addNewJurisdictionPriceRecord(item);
				}
				// Update
				else if (item.InUse == true && item.JurisdictionPriceId != 0 && item.IsUpdate == true)
				{
					dbC.updateJurisdictionPriceRecord(item);
				}
				// Delete -> Item is not in use and the jPrice id is not 0
				else if (item.InUse == false && item.JurisdictionPriceId != 0)
				{
					dbC.deleteJurisdictionPriceRecord(item.JurisdictionPriceId);
				}
			}

			return RedirectToAction("GetIdentifierList");
		}


		[HttpPost]
    [ValidateInput(false)]
    public ActionResult UpdateJurisdictionInfo(Master_ViewModel.MVM master)
		{
			dbC.updateJuridictionInfo(master.WLAVMMaster.JConfig);

			var iList = new IdentifierListParams();
			iList.ClientId = master.WLAVMMaster.JConfig.ClientId;
			iList.JurisdictionId = master.WLAVMMaster.JConfig.JurisdictionId;

			if (master.WLAVMMaster.JConfig.OutOfArea == true)
			{
				return RedirectToAction("GetIdentifierList", master.WLAVMMaster.ClientLookupModel);
			}
			else
			{
				return RedirectToAction("EditIdentifierList", iList);
			}


		}

		/// <summary>
		/// Edit the params in an out of area jurisdiction
		/// </summary>
		/// <param name="identifierListParams"></param>
		/// <returns></returns>
		[HttpGet]
		public ActionResult EditOutOfAreaJurisdiction(IdentifierListParams identifierListParams)
		{
			var master = new Master_ViewModel.MVM();
			master.WLAVMMaster = new WLA_ViewModel.WLAVM();
			master.WLAVMMaster.JConfig = new JurisdictionConfiguration();

			/// When editing the Default, there will never be any jurisdiction price config so we can skip all of this
			if (identifierListParams.ClientId.ToUpper() != "DEFAULT" && identifierListParams.JurisdictionId != 0)
			{
				var updateJurisInfo = dbC.GetUpdateJurisdictionInformation(identifierListParams.ClientId, identifierListParams.JurisdictionId);

				master.WLAVMMaster.JConfig = updateJurisInfo;
			}
			master.WLAVMMaster.ClientLookupModel = new ClientLookupModel();
			master.WLAVMMaster.ClientLookupModel.ClientId = identifierListParams.ClientId;

			master.WLAVMMaster.JConfig.JurisdictionId = identifierListParams.JurisdictionId;

			return View("UpdateJurisdictionInfo", master);
		}

		/// <summary>
		/// Shows the individual identifiers that can be edited for each jurisdiction
		/// </summary>
		/// <param name="identifierListParams"></param>
		/// <returns></returns>
		public ActionResult EditIdentifierList(IdentifierListParams identifierListParams)
		{
			///Set the user whom will be connecting to the DB
			var master = new Master_ViewModel.MVM();
			master.WLAVMMaster = new WLA_ViewModel.WLAVM();
			master.WLAVMMaster.JConfig = new JurisdictionConfiguration();

			/// When editing the Default, there will never be any jurisdiction price config so we can skip all of this
			if (identifierListParams.ClientId.ToUpper() != "DEFAULT" && identifierListParams.JurisdictionId != 0)
			{
				var updateJurisInfo = dbC.GetUpdateJurisdictionInformation(identifierListParams.ClientId, identifierListParams.JurisdictionId);

				master.WLAVMMaster.JConfig = updateJurisInfo;

				///This will all be moved into the jurisdiction price Info/Edit section
				//var templateJurisdiction = dbC.GetTemplateJurisdiction(identifierListParams.ClientId, identifierListParams.JurisdictionId);
				//master.WLAVMMaster.JConfig = templateJurisdiction;

				//var templateJPriceRecords = dbC.GetJurisdictionPriceRecords(identifierListParams.ClientId, identifierListParams.JurisdictionId);


				/////Add new template price records to get up to a max of 40 so we can add or remove records
				//for (var i = templateJPriceRecords.Count; i < 40; i++)
				//{
				//	templateJPriceRecords.Add(new JurisdictionPriceConfiguration()
				//	{
				//		InUse = false
				//	});
				//}

				//master.WLAVMMaster.JConfig.JurisdictionPrice = new List<JurisdictionPriceConfiguration>();
				//master.WLAVMMaster.JConfig.JurisdictionPrice = templateJPriceRecords;
			}


			master.WLAVMMaster.ClientLookupModel = new ClientLookupModel();
			master.WLAVMMaster.ClientLookupModel.ClientId = identifierListParams.ClientId;

			master.WLAVMMaster.JConfig.JurisdictionId = identifierListParams.JurisdictionId;

			master.WLAVMMaster.IdentifierVM = new IdentifierVM();
			master.WLAVMMaster.IdentifierVM = dbC.SetIdentifierList(identifierListParams);



			return View("IdentifierList", master);
		}


		[HttpGet]
		public ActionResult EditLockboxValidation(int validationId)
		{
			var master = new Master_ViewModel.MVM();
			master.WLAVMMaster = new WLA_ViewModel.WLAVM();
			master.WLAVMMaster.ClientLookupModel = new ClientLookupModel();
			master.WLAVMMaster.ClientLookupModel.ClientId = Session["ClientId"].ToString();

			master.WLAVMMaster.LBConfig = new LockboxConfiguration();
			master.WLAVMMaster.LBConfig = dbC.getIndividualLockboxRecord(validationId);

			return View("EditLockboxValidation", master);
		}

		[HttpPost]
		public ActionResult SetLockboxValidation(Master_ViewModel.MVM master)
		{
			if(master.WLAVMMaster.LBConfig.ChangeAll == true)
			{
				dbC.changeAllLockboxValidation(master.WLAVMMaster.LBConfig.WebClientValidationId, master.WLAVMMaster.LBConfig.Text);
			}
			else
			{
				dbC.changeOneLockboxValidation(master.WLAVMMaster.LBConfig.WebClientValidationId, master.WLAVMMaster.LBConfig.Text);
			}
			return RedirectToAction("GetIdentifierList");
		}

		/// <summary>
		/// Lookup and shows the form to edit individual identifiers
		/// </summary>
		/// <param name="lookup"></param>
		/// <returns></returns>
		[Route("Home/EditIndividualIdentifier")]
		public ActionResult EditIndividualIdentifier(EditIndividualIdentifierLookup lookup)
		{
			var identifier = dbC.getSingleIdentifier(lookup);

			if (lookup.ClientId.ToUpper() != "DEFAULT" && (identifier.WebClientId == 0 || identifier.JurisdictionId != lookup.JurisdictionId))
			{
				///this is a default and needs to be changed before saving to database
				identifier.WasDefault = true;
				identifier.Category = lookup.ClientId.ToUpper() + "-0";
				identifier.WebClientId = lookup.WebClientId;
				identifier.JurisdictionId = lookup.JurisdictionId;
			}

			///check each identifier to see if there is items in the placeholder table
			var placeholder = dbC.setPlaceholderList(identifier.WebClientConfigurationId);

			if (placeholder != null && placeholder.Count() > 0)
			{
				identifier.placeholder = placeholder;
				//Add more placeholder slots that will be hidden unless needed
				for (var i = 0; i < 50; i++)
				{
					identifier.placeholder.Add(new PlaceholderText
					{
						//We want it to have the same configuration ID as the rest
						WebClientConfigurationId = identifier.placeholder[0].WebClientConfigurationId,
						InUse = false
					});
				}
			}

			var master = new Master_ViewModel.MVM();
			master.WLAVMMaster = new WLA_ViewModel.WLAVM();
			master.WLAVMMaster.ClientLookupModel = new ClientLookupModel();
			master.WLAVMMaster.ClientLookupModel.ClientId = lookup.ClientId;

			master.WLAVMMaster.ClientConfig = new ClientConfiguration();
			master.WLAVMMaster.ClientConfig = identifier;
			master.WLAVMMaster.ClientConfig.JurisdictionId = lookup.JurisdictionId;

			//return Json(new { Url = "EditIndividualIdentifier", identifier });
			return View("EditIndividualIdentifier", master);
		}

		/// <summary>
		/// Form submit to edit identifiers
		/// </summary>
		/// <param name="row"></param>
		/// <returns></returns>
		[ValidateInput(false)]
		public ActionResult SetIndividualIdentifier(Master_ViewModel.MVM master)
		{

			if (master.WLAVMMaster.ClientConfig.WasDefault == true && master.WLAVMMaster.ClientConfig.WebClientId != 0)
			{
				///This is a new row so it will have a new identifier
				var insertedRowId = dbC.insertIdentifier(master.WLAVMMaster.ClientConfig);
				if (master.WLAVMMaster.ClientConfig.placeholder != null && master.WLAVMMaster.ClientConfig.placeholder.Count > 0)
				{
					foreach (var p in master.WLAVMMaster.ClientConfig.placeholder)
					{
						if (p.InUse != true) continue;
						dbC.insertPlaceholderRow(p, insertedRowId);
					}
				}
			}
			else
			{
				dbC.updateIdentifier(master.WLAVMMaster.ClientConfig);
				if (master.WLAVMMaster.ClientConfig.placeholder != null && master.WLAVMMaster.ClientConfig.placeholder.Count > 0)
				{
					foreach (var p in master.WLAVMMaster.ClientConfig.placeholder)
					{
						///Check to see if row already exists
						var exists = dbC.checkPlaceholderExistance(p.WebClientConfigPlaceholderId);

						if (exists && p.InUse == true)
						{
							///If it exists and InUse = Yes, UPDATE
							dbC.updatePlaceholderRow(p, master.WLAVMMaster.ClientConfig.WebClientConfigurationId);
						}
						else if (exists && p.InUse == false)
						{
							///IF exists and InUse  = No, DELETE
							dbC.removePlaceholder(p.WebClientConfigPlaceholderId);
						}
						else if (!exists && p.InUse == true)
						{
							///If doesn't exist and InUse = yes, CREATE
							dbC.insertPlaceholderRow(p, master.WLAVMMaster.ClientConfig.WebClientConfigurationId);
						}
						else if (!exists && p.InUse == false)
						{
							///If doesn't exist and inuse = no, CONTINUE
							continue;
						}
					}
				}
			}

			master.WLAVMMaster.IdentifierVM = dbC.SetIdentifierList(new IdentifierListParams
			{
				ClientId = master.WLAVMMaster.ClientConfig.ClientId,
				JurisdictionId = master.WLAVMMaster.ClientConfig.JurisdictionId
			});

			var iList = new IdentifierListParams();
			iList.ClientId = master.WLAVMMaster.ClientConfig.ClientId;
			iList.JurisdictionId = master.WLAVMMaster.ClientConfig.JurisdictionId;

			return RedirectToAction("EditIdentifierList", iList);
			//return View("IdentifierList", row);
		}

		/// <summary>
		/// Logic to delete individual identifiers
		/// Note:  Will not allow the deletion of default identifiers as they are for every client
		/// </summary>
		/// <param name="lookup"></param>
		/// <returns></returns>
		public ActionResult DeleteIndividualIdentifier(EditIndividualIdentifierLookup lookup)
		{
			///Make triple sure that they aren't trying to remove a default identifier
			var identifierFromDB = dbC.checkDefaultIdentifier(lookup);
			if (identifierFromDB.web_client_id == 0)
			{
				///this is a default record and we want to abort 
				return null;
			}
			else
			{
				///Must remove any placeholders first
				///We also need to delete any associated Placeholder Text Items
				var placeholder = dbC.setPlaceholderList(lookup.WebClientConfigurationId);
				if (placeholder != null && placeholder.Count > 0)
				{
					foreach (var row in placeholder)
					{
						dbC.removePlaceholder(row.WebClientConfigPlaceholderId);
					}
				}
				///Not a default and therefore can be removed from the DB
				dbC.removeIdentifier(lookup.WebClientConfigurationId);

			}
			var master = new Master_ViewModel.MVM();
			master.WLAVMMaster = new WLA_ViewModel.WLAVM();
			master.WLAVMMaster.ClientLookupModel = new ClientLookupModel();
			master.WLAVMMaster.ClientLookupModel.ClientId = lookup.ClientId;

			master.WLAVMMaster.IdentifierVM = new IdentifierVM();
			master.WLAVMMaster.IdentifierVM = dbC.SetIdentifierList(new IdentifierListParams
			{
				ClientId = lookup.ClientId,
				JurisdictionId = lookup.JurisdictionId
			});

			var iList = new IdentifierListParams();
			iList.ClientId = lookup.ClientId;
			iList.JurisdictionId = lookup.JurisdictionId;

			return RedirectToAction("EditIdentifierList", iList);

			//return View("IdentifierList", wlavm);
		}

		/// <summary>
		/// Method to test SQL submitted by form
		/// </summary>
		/// <param name="returnType"></param>
		/// <param name="returnText"></param>
		/// <returns></returns>
		[ValidateInput(false)]
		public ActionResult TestInput(string returnType, string returnText)
		{
			///Trim the return text so it doesn't have all the string stuff in it
			//returnText = returnText.Replace("$", "").Replace("\"", "");
			///Run the query that is passed in and return a datatable from the results
			///
			//var returnTable = wa.Database.SqlQuery<DataTable>(returnText);
			var dt = new DataTable();
			dt = dbC.testSQL(returnText, dt);

			return PartialView("_TestInput", dt);
		}
	}
}