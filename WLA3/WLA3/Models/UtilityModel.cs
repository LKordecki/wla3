﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WLA3.Models
{
	public class UtilityModel
	{
		public IEnumerable<AnimalType> animalType { get; set; }
		public IEnumerable<BreedInfo> breedInfo { get; set; }
		public IEnumerable<DropdownList> requestStatus { get; set; }
		public IEnumerable<RequestFormat> requestFormat { get; set; }
		public IEnumerable<DropdownList> requestType { get; set; }

		public IEnumerable<StateList> stateList { get; set; }

		public IEnumerable<DropdownList> sexList { get; set; }
		public IEnumerable<DropdownList> ageList { get; set; }
		public IEnumerable<DropdownList> coatList { get; set; }
		public IEnumerable<DropdownList> tailList { get; set; }
		public IEnumerable<DropdownList> sizeList { get; set; }
		public IEnumerable<DropdownList> colorList { get; set; }
		public IEnumerable<DropdownList> earList { get; set; }
		public IEnumerable<DropdownList> noseList { get; set; }
		public IEnumerable<DropdownList> collarType { get; set; }
		public IEnumerable<DropdownList> collarColor { get; set; }

		public class DropdownList
		{
			public string ddKey { get; set; }
			public string ddText { get; set; }
		}

		public class BreedInfo
		{
			public int breedKey { get; set; }
			public string breedShortName { get; set; }
			public string breedProperName { get; set; }
			public string breedGroup { get; set; }
			public string animalType { get; set; }
			public string species { get; set; }
		}

		public class AnimalType
		{
			public string animalTypeKey { get; set; }
			public string animalTypeText { get; set; }
		}

		public class StateList
		{
			public string stateKey { get; set; }
			public string stateText { get; set; }
		}


		public class RequestFormat
		{
			public string formatKey { get; set; }
			public string formatText { get; set; }
		}

		public class LookupArrray
		{
			public string lookupString { get; set; }
			public object lookupObject { get; set; }
		}
	}
}





