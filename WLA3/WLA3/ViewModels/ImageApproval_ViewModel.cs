﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WLA3.Models;

namespace WLA3.ViewModels
{
	public class ImageApproval_ViewModel
	{
		public List<ImageInformation> ImageList { get; set; }
		public UtilityModel DUM { get; set; }
		public int? TotalAnimalCount { get; set; }
	}
}