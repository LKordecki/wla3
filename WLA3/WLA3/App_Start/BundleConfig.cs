﻿using System.Web;
using System.Web.Optimization;

namespace WLA3
{
	public class BundleConfig
	{
		// For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
		public static void RegisterBundles(BundleCollection bundles)
		{
			bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
									"~/Scripts/jquery-{version}.js"));

			bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
									"~/Scripts/jquery.validate*",
									"~/Scripts/jquery.unobtrusive-ajax.min.js"));

			bundles.Add(new ScriptBundle("~/bundles/libraryScripts").Include(
						"~/Scripts/jquery.tablesorter.min.js",
						"~/Scripts/jquery.tablesorter.pager.js",
						"~/Scripts/codemirror-2.37/lib/codemirror.js",
						"~/Scripts/codemirror-2.37/mode/htmlmixed/htmlmixed.js",
						"~/Scripts/codemirror-2.37/mode/css/css.js",
						"~/Scripts/codemirror-2.37/mode/javascript/javascript.js",
						"~/Scripts/codemirror-2.37/mode/sql/sql.js",
						"~/Scripts/codemirror-2.37/mode/xml/xml.js",
						"~/Scripts/jquery.popupoverlay.js",
						"~/Scripts/fabric.2.3.3.min.js",
						"~/Scripts/jCanvas.js"
						));

			bundles.Add(new ScriptBundle("~/bundles/custom").Include(
			"~/Scripts/Custom.js"));

			// Use the development version of Modernizr to develop with and learn from. Then, when you're
			// ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
			bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
									"~/Scripts/modernizr-*"));

			bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
								"~/Scripts/bootstrap.js",
								"~/Scripts/respond.js"));

			bundles.Add(new StyleBundle("~/Content/css").Include(
								"~/Content/bootstrap.css",
								"~/Content/font-awesome.min.css",
								"~/Content/site.css",
								"~/Scripts/codemirror-2.37/lib/codemirror.css"));
		}
	}
}
