﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static WLA3.Models.CustomModels;

namespace WLA3.ViewModels
{
	public class Master_ViewModel
	{
		public class MVM
		{
			public ClientLookupModel CLM { get; set; }
			public WLA_ViewModel.WLAVM WLAVMMaster { get; set; }
			public ImageApproval_ViewModel ImageApprovalMaster { get; set; }

			public PH3Config_ViewModel PH3Config { get; set; }
		}
	}
}