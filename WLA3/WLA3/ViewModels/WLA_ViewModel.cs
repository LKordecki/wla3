﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WLA3.Models;

namespace WLA3.ViewModels
{
	public class WLA_ViewModel
	{ 
		public class WLAVM
		{
			public CustomModels.ClientLookupModel ClientLookupModel { get; set; }
			public List<CustomModels.IdentifierLookupList> CM_ILL { get; set; }
			public CustomModels.IdentifierVM IdentifierVM { get; set; }
			public ClientConfiguration ClientConfig { get; set; }
			public ValidConfiguration ValidConfig { get; set; }
			public JurisdictionConfiguration JConfig { get; set; }
			public List<JurisdictionPriceConfiguration> JPriceConfig { get; set; }

			public List<JurisdictionConfiguration> JInfo { get; set; }

			public List<LockboxConfiguration> LBInfo { get; set; }
			public LockboxConfiguration LBConfig { get; set; }
		}
	}
}