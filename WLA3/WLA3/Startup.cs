﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(WLA3.Startup))]
namespace WLA3
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
