﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WLA3.Models;
using WLA3.ViewModels;
using static WLA3.Models.CustomModels;

namespace WLA3.Controllers
{
	public class HomeController : Controller
	{
		DatabaseController dbC = new DatabaseController();

		public ActionResult Index() => View(new Master_ViewModel.MVM());

		[HttpPost]
		public ActionResult RedirectToApp([Bind(Prefix = "CLM")] ClientLookupModel clientInfo, string selectApp)
		{

			//var wlavm = new WLA_ViewModel.WLAVM();
			//wlavm.ClientLookupModel = new ClientLookupModel();
			////wlavm.ClientLookupModel = clientInfo;

			//if (clientInfo != null)
			//{
			//	wlavm.ClientLookupModel.User = clientInfo.User;
			//	wlavm.ClientLookupModel.Password = clientInfo.Password;
			//	wlavm.ClientLookupModel.ClientId = clientInfo.ClientId;
			//}
			if(string.IsNullOrEmpty(Session["User"] as string) || (!string.IsNullOrEmpty(clientInfo.User) && Session["User"].ToString() != clientInfo.User))
      {
			Session["User"] = clientInfo.User;
      }
			if (string.IsNullOrEmpty(Session["Password"] as string) || (!string.IsNullOrEmpty(clientInfo.Password) && Session["Password"].ToString() != clientInfo.Password))
			{
				Session["Password"] = clientInfo.Password;
			}
			if (string.IsNullOrEmpty(Session["ClientId"] as string) || (!string.IsNullOrEmpty(clientInfo.ClientId)  && Session["ClientId"].ToString() != clientInfo.ClientId))
			{
				Session["ClientId"] = clientInfo.ClientId;
			}


			switch (selectApp)
			{
				case "WLA3":
					return RedirectToAction("GetIdentifierList", "WLA");
				case "ImageApproval":
					return RedirectToAction("GetPendingImages", "ImageApproval");
				case "Petharbor3":
					return RedirectToAction("Index", "PH3Config");
				default:
					break;
			}



			return null;
		}

	}
}