﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WLA3.Models
{
	public class CustomModels
	{
		public class ClientLookupModel
		{
			public string ClientId { get; set; }
			//[Required]
			[Display(Name ="SQL connection user id:")]
			public string User { get; set; }
			//[Required]
			[Display(Name ="SQL connection password:")]
			public string Password { get; set; }
		}

		public class IdentifierLookupList
		{
			public int JurisdictionId { get; set; }
			public string ClientId { get; set; }
			public string JurisdictionName { get; set; }
			public string JurisdictionZipcodes { get; set; }
			public bool? OutOfArea { get; set; }
		}
		public class PlaceholderText
		{
			public int WebClientConfigurationId { get; set; }
			public int WebClientConfigPlaceholderId { get; set; }
			public string Key { get; set; }
			public string Value { get; set; }
			public byte? SortOrder { get; set; }
			public bool InUse { get; set; }
		}

		public class IdentifierListParams
		{
			public string ClientId { get; set; }
			public string ActionType { get; set; }
			public int JurisdictionId { get; set; }
		}

		public class EditIndividualIdentifierLookup
		{
			public int WebClientId { get; set; }
			public int WebClientConfigurationId { get; set; }
			public string ClientId { get; set; }
			public int JurisdictionId { get; set; }
		}

		public class IdentifierVM
		{
			public string ClientId { get; set; }
			public int? JurisdictionId { get; set; }
			public int? WebClientId { get; set; }
			public List<f_wl_config_Result> ConfigResults { get; set; }
			public List<f_wl_config_valid_Result> ValidResults { get; set; }
		}
	}

	public class ClientConfiguration
	{
		public string ClientId { get; set; }
		public int WebClientConfigurationId { get; set; }
		[Display(Name = "Config Type: V = Validation, G = GUI Element")]
		public string ConfigType { get; set; }
		[Display(Name = "Web Client Id")]
		public int WebClientId { get; set; }
		[Display(Name = "Jurisdiction Id")]
		public int JurisdictionId { get; set; }
		[Display(Name = "Category")]
		public string Category { get; set; }
		[Display(Name ="Indentifier Description")]
		public string Description { get; set; }
		[Display(Name = "Identifier")]
		public string Identifier { get; set; }
		[Display(Name = "Display")]
		public bool DisplayInView { get; set; }
		[Display(Name = "Required")]
		public bool RequiredField { get; set; }
		[Display(Name = "Allow Change")]
		public bool AllowChange { get; set; }
		[Display(Name = "Return Type")]
		public string ReturnType { get; set; }
		[Display(Name = "Custom Text")]
		public string CustomText { get; set; }
		[Display(Name = "Placeholder Text")]
		public string PlaceholderText { get; set; }
		[Display(Name = "Regex Validation Rule")]
		public string Regex { get; set; }
		[Display(Name = "Regex Validation Error Message")]
		public string ValidationError { get; set; }
		[Display(Name = "Sort Order")]
		public byte? SortOrder { get; set; }
		[Display(Name = "Max Length")]
		public int? MaxLength { get; set; }
		[Display(Name ="Help HTML")]
		public string HelpHTML { get; set; }

		public bool WasDefault { get; set; }

		public List<CustomModels.PlaceholderText> placeholder { get; set; }
	}

	public class ValidConfiguration
	{
		public string ClientId { get; set; }
		public int WebClientConfigurationId { get; set; }
		[Display(Name = "Config Type: V = Validation, G = GUI Element")]
		public string ConfigType { get; set; }
		[Display(Name = "Web Client Id")]
		public int WebClientId { get; set; }
		[Display(Name = "Jurisdiction Id")]
		public int JurisdictionId { get; set; }
		[Display(Name = "Category")]
		public string Category { get; set; }
		[Display(Name = "Identifier")]
		public string Identifier { get; set; }
		[Display(Name = "Custom Text")]
		public string CustomText { get; set; }

		public bool WasDefault { get; set; }
	}

	public class JurisdictionConfiguration
	{
		public string ClientId { get; set; }
		public int JurisdictionId { get; set; }
		public string Jurisdiction { get; set; }
		public string JurisdictionTitle { get; set; }
		public string JurisdictionDescription { get; set; }
		public string Zipcodes { get; set; }
		public bool OutOfArea { get; set; }
		public string OutOfAreaURL { get; set; }
		public List<JurisdictionPriceConfiguration> JurisdictionPrice { get; set; }
	}

	public class LockboxConfiguration
	{
		public string ClientId { get; set; }
		public int? WebClientId { get; set; }
		public int WebClientValidationId { get; set; }
		public string Category { get; set; }
		public string Identifier { get; set; }
		[Required]
		public string Text { get; set; }
		public bool ChangeAll { get; set; }
	}

	public class JurisdictionPriceConfiguration
	{
		public string ClientId { get; set; }
		public int? JurisdictionId { get; set; }
		public bool InUse { get; set; }
		public bool IsUpdate { get; set; }
		public int JurisdictionPriceId { get; set; }
		public string ProcessingType { get; set; }
		public string Code { get; set; }
		public string PriceOrPercent { get; set; }
		public decimal? Price { get; set; }
		public decimal? PriceMin { get; set; }
		public decimal? PriceMax { get; set; }
		public decimal? PricePercent { get; set; }
		public int? TagTerm { get; set; }
		public int? TagTermAvailable { get; set; }
		[Required]
		public string JuvenileTag { get; set; }
		public string Description { get; set; }
		public string Disclaimer { get; set; }
		public string AvailabilityNew { get; set; }
		public string AvailabilityRenew { get; set; }
		public string PerTransOrItem { get; set; }
		public DateTime? DateEffective { get; set; }
		public DateTime? DateExpires { get; set; }
		[Required]
		public string DisplayJurisdiction { get; set; }
		[Required]
		public string DisplayLicenseSelect { get; set; }
		[Required]
		public int SortOrder { get; set; }
	}

	public class IdentifierCount
	{
		public string Identifier { get; set; }
		public int Count { get; set; }
	}

}