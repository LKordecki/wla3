﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WLA3.Models
{
  public class PH3ConfigModel
  {

    public class PH3Configs
    {
      public List<PH3ConfigRow> ConfigRows { get; set; }
    }

    public class PH3ConfigRequired
    {

      [Display(Name = "Would you like to show your 'Adoptable' animals?")]
      public string AdoptableDisplay { get; set; }

      [Required]
      [Display(Name ="Enter the short name for your link. This will be what patrons type into the URL box to access your page. (ex. BrowardAnimals, use _ instead of spaces)")]
      public string URLName { get; set; }
      [Display(Name = "Enter text below to rename the tab. If left blank, the default tab will say 'Adopt'.")]
      public string TextAdopt { get; set; }


      [Display(Name ="Would you like to show your 'Reported Found' animals? They will be displayed under a different tab than your adoptable animals.")]
      public string ReportedFoundURLDisplay { get; set; }

      [Required]
      [Display(Name ="If you selected 'Yes' above: Enter the short name for your found pets. You will be able to access your found pets directly with this link. (ex. BrowardFound, use _ instead of spaces) ")]
      public string ReportedFoundURLName { get; set; }
      //public string DisplayShowFound { get; set; }
      [Display(Name ="Enter text below to rename the tab. If left blank, the default tab will say 'Reported Found'.")]
      public string TextShowFound { get; set; }


      [Display(Name = "Would you like to show your 'Reported Lost' animals? These are animals reported by the public through the Breed Request Reports. They will be displayed under a different tab than your adoptable animals.")]
      public string ReportedLostURLDisplay { get; set; }

      [Required]
      [Display(Name = "If you selected 'Yes' above: Enter the short name for your lost pets. You will be able to access your lost pets directly with this link.  (ex. BrowardLost, use _ instead of spaces) ")]
      public string ReportedLostURLName { get; set; }
      //public string DisplayShowLost { get; set; }
      [Display(Name = "Enter text below to rename the tab. If left blank, the default tab will say 'Reported Lost'.")]
      public string TextShowLost { get; set; }

      [Required]
      [Display(Name ="Enter the shelter/facility list you would like included in this result set. If you have more than one shelter/facility, separate the codes with commas. (ex. HLP, HLP1, HLP2)")]
      public string ShelterList { get; set; }
    }

    public class PH3ConfigOptional
    {
      [Display(Name ="Display a header at the top of the page?")]
      public string DisplayHeaderHTML {get; set;}
      [Display(Name ="If you selected 'Display' above: Enter the custom HTML for your header below. If left blank, the header will default to the PetHarbor Logo.")]
      public string TextHeaderHTML { get; set; }

      [Display(Name ="Display custom title for the page? Title will appear between your header and menu bar.")]
      public string DisplayShelterTitle { get; set; }
      [Display(Name ="If you selected 'Display' above: Enter the text you would like displayed below.")]
      public string ShelterTitleText { get; set; }


      [Display(Name ="Display a custom footer at the bottom of the page?")]
      public string DisplayFooterHTML { get; set; }
      [Display(Name ="If you selected 'Display' above: Enter the custom HTML for your footer below. If left blank, no text will appear.")]
      public string TextFooterHTML { get; set; }


      [Display(Name ="Would you like to use custom CSS?")]
      public string DisplayCustomCSS { get; set; }
      [Display(Name ="If you selected 'Yes': Enter the direct URL to your css file below. If left blank, we will default to the grey/white color scheme.")]
      public string TextCustomCSS { get; set; }



      [Display(Name ="Dog: Allow patrons to sort by this animal type.")]
      public string DisplayShowDog { get; set; }
      [Display(Name = "Dog: Change the label on this button. Default is 'Dogs'.")]
      public string TextShowDog { get; set; }
      [Display(Name = "Cat: Allow patrons to sort by this animal type.")]
      public string DisplayShowCat { get; set; }
      [Display(Name = "Cat: Change the label on this button. Default is 'Cats'.")]
      public string TextShowCat { get; set; }
      [Display(Name = "Other: Allow patrons to sort by this animal type.")]
      public string DisplayShowOther { get; set; }
      [Display(Name = "Other: Change the label on this button. Default is 'Other'.")]
      public string TextShowOther { get; set; }



      [Display(Name ="Allow extra filters. If you select 'Yes', you will be able to turn on/off specific filters below. (ex. Breed, Age, Size, Color, Sort By, etc.)")]
      public string DisplayAllowExtraFilter { get; set; }

      [Display(Name = "Filter: Breed Selection. If your shelter/facility does not use breeds, or uses the 'Mixed Breed' text, select 'No'. ")]
      public string DisplayAllowSelectBreeds { get; set; }
      [Display(Name ="Filter: Breed Selection text. If left blank, we will default to 'Select Specific Breeds'. ")]
      public string TextAllowSelectBreeds { get; set; }

      [Display(Name ="Filter: Sort By. Allow patrons to sort the animals by different criteria.")]
      public string DisplayFilterSortBy { get; set; }
      [Display(Name = "Filter: Sort By text. If left blank, we will default to 'Sort By'. ")]
      public string TextFilterSortBy { get; set; }


      [Display(Name = "Sort By: Breed. Allow patrons to sort by breeds from A -> Z or Z -> A.")]
      public string DisplayAllowSortBreed { get; set; }
      [Display(Name ="Sort By: Days out. Allow patrons to sort by number of days pet has been in the shelter from Min -> Max or Max -> Min.")]
      public string DisplayAllowFilterDaysOut { get; set; }
      [Display(Name = "Sort By: Animal ID. Allow patrons to sort by animal ID/Name from 0 -> Z or Z -> 0.")]
      public string DisplayAllowSortID { get; set; }
      [Display(Name = "Filter: Age. Allow patrons to filter by 'Young' or 'Adult'. ")]
      public string DisplayFilterAge { get; set; }
      [Display(Name ="Filter: Gender. Allow patrons to filter by 'Male' or 'Female' which includes both intact and neutered/spayed animals.")]
      public string DisplayFilterGender { get; set; }
      [Display(Name ="Filter: Size. Allow patrons to filter by animal size. Feature still under development.")]
      public string DisplayFilterSize { get; set; }
      [Display(Name ="Filter: Color. Allow patrons to filter by animal color. Feature still under development.")]
      public string DisplayFilterColor { get; set; }


      [Display(Name ="Adoption Request: Allow your patrons to fill out an Adoptable Animal Request to receive emails when animals become adoptable through your shelter/facility. (Adopt tab only)")]
      public string AllowAdoptReport { get; set; }

      [Display(Name = "Lost Request: Allow your patrons to fill out a Lost Animal Request to receive emails when animals matching their lost pet become available through your shelter/facility. (Reported Found tab only)")]
      public string AllowLostReport { get; set; }
      [Display(Name = "Found Request: Allow your patrons to fill out a Found Animal Report to list animals they have found in the community on your shelter/facility results page. (Reported Lost tab only)")]
      public string AllowFoundReport { get; set; }

      //shelter title
      //shelter title link

      /// Dont forget to update breed req so they only use the shelter selected by code for these
      /// probably need to hook up footer
      /// Finish backend form submit
      /// 

    }

    public class PH3ConfigRow
    {
      public int PH3ClientResultsViewidn { get; set; }
      public string Identifier { get; set; }
      public bool Display { get; set; }
      public string CustomText { get; set; }

    }
  }
}