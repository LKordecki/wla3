﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WLA3.Models;
using WLA3.ViewModels;

namespace WLA3.Controllers
{
  public class PH3ConfigController : Controller
  {
    DatabaseController dbC = new DatabaseController();

    internal void SetupUNPW()
    {
      var master = new Master_ViewModel.MVM();
      master.CLM = new CustomModels.ClientLookupModel();
      master.CLM.User = Session["User"].ToString();
      master.CLM.Password = Session["Password"].ToString();
      master.CLM.ClientId = Session["ClientId"].ToString();

      if (master.CLM == null || (master.CLM.User == null || master.CLM.Password == null || master.CLM.ClientId == null))
      {
        if (master.CLM == null)
          master.CLM = new CustomModels.ClientLookupModel();
        var unpw = dbC.GetUNPW();
        master.CLM.User = unpw.User;
        master.CLM.Password = unpw.Password;
        master.CLM.ClientId = unpw.ClientId;
      }

      dbC.SetUNPW("Animal", master.CLM.User, master.CLM.Password, master.CLM.ClientId);

    }

    // GET: PH3Config
    public ActionResult Index()
    {
      SetupUNPW();
      var master = new Master_ViewModel.MVM();
      //master.CLM = new CustomModels.ClientLookupModel();
      //master.CLM.User = Session["User"].ToString();
      //master.CLM.Password = Session["Password"].ToString();
      //master.CLM.ClientId = Session["ClientId"].ToString();

      //if (master.CLM == null || (master.CLM.User == null || master.CLM.Password == null || master.CLM.ClientId == null))
      //{
      //  if (master.CLM == null)
      //    master.CLM = new CustomModels.ClientLookupModel();
      //  var unpw = dbC.GetUNPW();
      //  master.CLM.User = unpw.User;
      //  master.CLM.Password = unpw.Password;
      //  master.CLM.ClientId = unpw.ClientId;
      //}

      //dbC.SetUNPW("Animal", master.CLM.User, master.CLM.Password, master.CLM.ClientId);


      var ph3MasterView = new PH3Config_ViewModel();
      ph3MasterView.required = new PH3ConfigModel.PH3ConfigRequired();
      ph3MasterView.optional = new PH3ConfigModel.PH3ConfigOptional();

      var mvm = new Master_ViewModel.MVM();
      mvm.PH3Config = new PH3Config_ViewModel();
      mvm.PH3Config = ph3MasterView;
      return View(mvm);
    }

    public ActionResult ConfigError(string message)
    {

      return View(message);
    }


    [ValidateInput(false)]
    public ActionResult EditPH3Configurations(Master_ViewModel.MVM model)
    {
      SetupUNPW();
      var adoptidn = 0;
      var lostidn = 0;
      var foundidn = 0;

      if (model == null || model.PH3Config == null || model.PH3Config.required == null || model.PH3Config.optional == null)
      {
        /// Check to make sure this works...
        var message = "Models were not processed correctly.";
        return View("ConfigError", message);
      }

      var required = model.PH3Config.required;
      var optional = model.PH3Config.optional;

      /// Check to make sure all the URL Names they picked are available.
      if (required.AdoptableDisplay == "Y")
      {
        /// Yes - Check to see if name is available
        var nameAvailable = CheckURLNameAvailability(required.URLName);
        if (nameAvailable == "N")
        {
          var message = "Failed trying to insert duplicate URL Name for Adoptable animals. Please pick different URL Name and try again.";
          return View("ConfigError", message);
        }
      }
      if (required.ReportedLostURLDisplay == "Y")
      {
        var nameAvailable = CheckURLNameAvailability(required.ReportedLostURLName);
        if (nameAvailable == "N")
        {
          var message = "Failed trying to insert duplicate URL Name for Lost animals. Please pick different URL Name and try again.";
          return View("ConfigError", message);
        }
      }
      if (required.ReportedFoundURLDisplay == "Y")
      {
        var nameAvailable = CheckURLNameAvailability(required.ReportedFoundURLName);
        if (nameAvailable == "N")
        {
          var message = "Failed trying to insert duplicate URL Name for Found animals. Please pick different URL Name and try again.";
          return View("ConfigError", message);
        }
      }

      /// Get shelter list and take first value for client info
      var shelterList = new List<string>();

      if (!string.IsNullOrEmpty(required.ShelterList))
      {
        var splitList = required.ShelterList.Split(',');
        foreach (var item in splitList)
        {
          shelterList.Add(item.TrimStart().TrimEnd());
        }
      }

      /// All the names are valid, is if they selected 'Display' we add them to the table and record their IDNs
      if (required.AdoptableDisplay == "Y")
      {
        adoptidn = dbC.CreateClientResultsViewRow("adopt", required.URLName, shelterList[0]);
      }
      if (required.ReportedLostURLDisplay == "Y")
      {
        lostidn = dbC.CreateClientResultsViewRow("lost", required.ReportedLostURLName, shelterList[0]);
      }
      if (required.ReportedFoundURLDisplay == "Y")
      {
        foundidn = dbC.CreateClientResultsViewRow("found", required.ReportedFoundURLName, shelterList[0]);
      }

      /// Create the shelter list for each of these new idns
      if (adoptidn != 0)
      {
        foreach (var item in shelterList)
        {
          dbC.InsertPH3Shelter(adoptidn, item);
        }
      }
      if (lostidn != 0)
      {
        foreach (var item in shelterList)
        {
          dbC.InsertPH3Shelter(lostidn, item);
        }
      }
      if (foundidn != 0)
      {
        foreach (var item in shelterList)
        {
          dbC.InsertPH3Shelter(foundidn, item);
        }
      }

      /// Create model List where we will store all of the new configs before inserting them into the db
      var configList = new PH3ConfigModel.PH3Configs();
      configList.ConfigRows = new List<PH3ConfigModel.PH3ConfigRow>();

      /// Create our Adopt/Lost/Found links to link them all together
      if (adoptidn != 0)
      {
        configList.ConfigRows.Add(new PH3ConfigModel.PH3ConfigRow
        {
          PH3ClientResultsViewidn = adoptidn,
          Identifier = "AdoptLink",
          Display = true,
          CustomText = required.URLName
        });
        if (lostidn != 0)
        {
          configList.ConfigRows.Add(new PH3ConfigModel.PH3ConfigRow
          {
            PH3ClientResultsViewidn = adoptidn,
            Identifier = "LostLink",
            Display = true,
            CustomText = required.ReportedLostURLName
          });
        }
        if (foundidn != 0)
        {
          configList.ConfigRows.Add(new PH3ConfigModel.PH3ConfigRow
          {
            PH3ClientResultsViewidn = adoptidn,
            Identifier = "FoundLink",
            Display = true,
            CustomText = required.ReportedFoundURLName
          });
        }
      }
      if (lostidn != 0)
      {
        configList.ConfigRows.Add(new PH3ConfigModel.PH3ConfigRow
        {
          PH3ClientResultsViewidn = lostidn,
          Identifier = "LostLink",
          Display = true,
          CustomText = required.ReportedLostURLName
        });
        if (adoptidn != 0)
        {
          configList.ConfigRows.Add(new PH3ConfigModel.PH3ConfigRow
          {
            PH3ClientResultsViewidn = lostidn,
            Identifier = "AdoptLink",
            Display = true,
            CustomText = required.URLName
          });
        }
        if (foundidn != 0)
        {
          configList.ConfigRows.Add(new PH3ConfigModel.PH3ConfigRow
          {
            PH3ClientResultsViewidn = lostidn,
            Identifier = "FoundLink",
            Display = true,
            CustomText = required.ReportedFoundURLName
          });
        }
      }
      if (foundidn != 0)
      {
        configList.ConfigRows.Add(new PH3ConfigModel.PH3ConfigRow
        {
          PH3ClientResultsViewidn = foundidn,
          Identifier = "FoundLink",
          Display = true,
          CustomText = required.ReportedFoundURLName
        });
        if (adoptidn != 0)
        {
          configList.ConfigRows.Add(new PH3ConfigModel.PH3ConfigRow
          {
            PH3ClientResultsViewidn = foundidn,
            Identifier = "AdoptLink",
            Display = true,
            CustomText = required.URLName
          });
        }
        if (lostidn != 0)
        {
          configList.ConfigRows.Add(new PH3ConfigModel.PH3ConfigRow
          {
            PH3ClientResultsViewidn = foundidn,
            Identifier = "LostLink",
            Display = true,
            CustomText = required.ReportedLostURLName
          });
        }
      }

      /// Get all the default configurations so we can compare the selected ones
      SetupUNPW();
      var defaultTable = dbC.getShelterDisplayPreferences(1);
      /// Compare the defaults to the model and see if they are different for each identifier - someday this could be split out?
      //--------------------//
      var isSame = true;
      /// HeaderHTML
      isSame = compareDefaults(defaultTable.Rows.Cast<DataRow>().Where(id => id.Field<string>("identifier") == "HeaderHTML").FirstOrDefault(),
        optional.DisplayHeaderHTML == "Y" ? true : false,
        optional.TextHeaderHTML);
      if (!isSame)
      {
        LoadConfigList(configList.ConfigRows, "HeaderHTML", optional.DisplayHeaderHTML == "Y" ? true : false, optional.TextHeaderHTML,
          adoptidn, lostidn, foundidn);
      }
      //--------------------//
      isSame = true;
      /// CustomCSS
      isSame = compareDefaults(defaultTable.Rows.Cast<DataRow>().Where(id => id.Field<string>("identifier") == "CustomCSS").FirstOrDefault(),
        optional.DisplayCustomCSS == "Y" ? true : false,
        optional.TextCustomCSS);
      if (!isSame)
      {
        LoadConfigList(configList.ConfigRows, "CustomCSS", optional.DisplayCustomCSS == "Y" ? true : false, optional.TextCustomCSS,
          adoptidn, lostidn, foundidn);
      }
      //--------------------//
      isSame = true;
      /// ShowAdopt
      isSame = compareDefaults(defaultTable.Rows.Cast<DataRow>().Where(id => id.Field<string>("identifier") == "ShowAdopt").FirstOrDefault(),
        required.AdoptableDisplay == "Y" ? true : false,
        required.TextAdopt);
      if (!isSame)
      {
        LoadConfigList(configList.ConfigRows, "ShowAdopt", required.AdoptableDisplay == "Y" ? true : false, required.TextAdopt,
          adoptidn, lostidn, foundidn);
      }
      //--------------------//
      isSame = true;
      /// ShowLost
      isSame = compareDefaults(defaultTable.Rows.Cast<DataRow>().Where(id => id.Field<string>("identifier") == "ShowLost").FirstOrDefault(),
        required.ReportedLostURLDisplay == "Y" ? true : false,
        required.TextShowLost);
      if (!isSame)
      {
        LoadConfigList(configList.ConfigRows, "ShowLost", required.ReportedLostURLDisplay == "Y" ? true : false, required.TextShowLost,
          adoptidn, lostidn, foundidn);
      }
      //--------------------//
      isSame = true;
      /// ShowFound
      isSame = compareDefaults(defaultTable.Rows.Cast<DataRow>().Where(id => id.Field<string>("identifier") == "ShowFound").FirstOrDefault(),
        required.ReportedFoundURLDisplay == "Y" ? true : false,
        required.TextShowFound);
      if (!isSame)
      {
        LoadConfigList(configList.ConfigRows, "ShowFound", required.ReportedFoundURLDisplay == "Y" ? true : false, required.TextShowFound,
          adoptidn, lostidn, foundidn);
      }
      //--------------------//
      isSame = true;
      /// ShowDog
      isSame = compareDefaults(defaultTable.Rows.Cast<DataRow>().Where(id => id.Field<string>("identifier") == "ShowDog").FirstOrDefault(),
        optional.DisplayShowDog == "Y" ? true : false,
        optional.TextShowDog);
      if (!isSame)
      {
        LoadConfigList(configList.ConfigRows, "ShowDog", optional.DisplayShowDog == "Y" ? true : false, optional.TextShowDog,
          adoptidn, lostidn, foundidn);
      }
      //--------------------//
      isSame = true;
      /// ShowCat
      isSame = compareDefaults(defaultTable.Rows.Cast<DataRow>().Where(id => id.Field<string>("identifier") == "ShowCat").FirstOrDefault(),
        optional.DisplayShowCat == "Y" ? true : false,
        optional.TextShowCat);
      if (!isSame)
      {
        LoadConfigList(configList.ConfigRows, "ShowCat", optional.DisplayShowCat == "Y" ? true : false, optional.TextShowCat,
          adoptidn, lostidn, foundidn);
      }
      //--------------------//
      isSame = true;
      /// ShowOther
      isSame = compareDefaults(defaultTable.Rows.Cast<DataRow>().Where(id => id.Field<string>("identifier") == "ShowOther").FirstOrDefault(),
        optional.DisplayShowOther == "Y" ? true : false,
        optional.TextShowOther);
      if (!isSame)
      {
        LoadConfigList(configList.ConfigRows, "ShowOther", optional.DisplayShowOther == "Y" ? true : false, optional.TextShowOther,
          adoptidn, lostidn, foundidn);
      }
      //--------------------//
      isSame = true;
      /// AllowFilterSortBy
      isSame = compareDefaults(defaultTable.Rows.Cast<DataRow>().Where(id => id.Field<string>("identifier") == "AllowFilterSortBy").FirstOrDefault(),
        optional.DisplayFilterSortBy == "Y" ? true : false,
        optional.TextFilterSortBy);
      if (!isSame)
      {
        LoadConfigList(configList.ConfigRows, "AllowFilterSortBy", optional.DisplayFilterSortBy == "Y" ? true : false, optional.TextFilterSortBy,
          adoptidn, lostidn, foundidn);
      }
      //--------------------//
      isSame = true;
      /// AllowFilterDaysOut
      isSame = compareDefaults(defaultTable.Rows.Cast<DataRow>().Where(id => id.Field<string>("identifier") == "AllowFilterDaysOut").FirstOrDefault(),
        optional.DisplayAllowFilterDaysOut == "Y" ? true : false,
        null);
      if (!isSame)
      {
        LoadConfigList(configList.ConfigRows, "AllowFilterDaysOut", optional.DisplayAllowFilterDaysOut == "Y" ? true : false, null,
          adoptidn, lostidn, foundidn);
      }
      //--------------------//
      isSame = true;
      /// AllowSelectBreeds
      isSame = compareDefaults(defaultTable.Rows.Cast<DataRow>().Where(id => id.Field<string>("identifier") == "AllowSelectBreeds").FirstOrDefault(),
        optional.DisplayAllowSelectBreeds == "Y" ? true : false,
        optional.TextAllowSelectBreeds);
      if (!isSame)
      {
        LoadConfigList(configList.ConfigRows, "AllowSelectBreeds", optional.DisplayAllowSelectBreeds == "Y" ? true : false, optional.TextAllowSelectBreeds,
          adoptidn, lostidn, foundidn);
      }
      //--------------------//
      //isSame = true;
      ///// AllowSortDistance
      //isSame = compareDefaults(defaultTable.Rows.Cast<DataRow>().Where(id => id.Field<string>("identifier") == "AllowSortDistance").FirstOrDefault(),
      //  optional.sort == "Y" ? true : false,
      //  optional.TextAllowSelectBreeds);
      //if (!isSame)
      //{
      //  LoadConfigList(configList.ConfigRows, "AllowSortDistance", optional.DisplayAllowFilterDaysOut == "Y" ? true : false, optional.TextAllowSelectBreeds,
      //    adoptidn, lostidn, foundidn);
      //}
      //--------------------//
      isSame = true;
      /// AllowSortBreed
      isSame = compareDefaults(defaultTable.Rows.Cast<DataRow>().Where(id => id.Field<string>("identifier") == "AllowSortBreed").FirstOrDefault(),
        optional.DisplayAllowSortBreed == "Y" ? true : false,
        null);
      if (!isSame)
      {
        LoadConfigList(configList.ConfigRows, "AllowSortBreed", optional.DisplayAllowSortBreed == "Y" ? true : false, null,
          adoptidn, lostidn, foundidn);
      }
      //--------------------//
      isSame = true;
      /// AllowSortID
      isSame = compareDefaults(defaultTable.Rows.Cast<DataRow>().Where(id => id.Field<string>("identifier") == "AllowSortID").FirstOrDefault(),
        optional.DisplayAllowSortID == "Y" ? true : false,
        null);
      if (!isSame)
      {
        LoadConfigList(configList.ConfigRows, "AllowSortID", optional.DisplayAllowSortID == "Y" ? true : false, null,
          adoptidn, lostidn, foundidn);
      }
      //--------------------//
      isSame = true;
      /// AllowFilterAge
      isSame = compareDefaults(defaultTable.Rows.Cast<DataRow>().Where(id => id.Field<string>("identifier") == "AllowFilterAge").FirstOrDefault(),
        optional.DisplayFilterAge == "Y" ? true : false,
        null);
      if (!isSame)
      {
        LoadConfigList(configList.ConfigRows, "AllowFilterAge", optional.DisplayFilterAge == "Y" ? true : false, null,
          adoptidn, lostidn, foundidn);
      }
      //--------------------//
      isSame = true;
      /// AllowFilterAge
      isSame = compareDefaults(defaultTable.Rows.Cast<DataRow>().Where(id => id.Field<string>("identifier") == "AllowFilterGender").FirstOrDefault(),
        optional.DisplayFilterGender == "Y" ? true : false,
        null);
      if (!isSame)
      {
        LoadConfigList(configList.ConfigRows, "AllowFilterGender", optional.DisplayFilterGender == "Y" ? true : false, null,
          adoptidn, lostidn, foundidn);
      }
      //--------------------//
      isSame = true;
      /// AllowFilterSize
      isSame = compareDefaults(defaultTable.Rows.Cast<DataRow>().Where(id => id.Field<string>("identifier") == "AllowFilterSize").FirstOrDefault(),
        optional.DisplayFilterSize == "Y" ? true : false,
        null);
      if (!isSame)
      {
        LoadConfigList(configList.ConfigRows, "AllowFilterSize", optional.DisplayFilterSize == "Y" ? true : false, null,
          adoptidn, lostidn, foundidn);
      }
      //--------------------//
      isSame = true;
      /// AllowFilterColor
      isSame = compareDefaults(defaultTable.Rows.Cast<DataRow>().Where(id => id.Field<string>("identifier") == "AllowFilterColor").FirstOrDefault(),
        optional.DisplayFilterColor == "Y" ? true : false,
        null);
      if (!isSame)
      {
        LoadConfigList(configList.ConfigRows, "AllowFilterColor", optional.DisplayFilterColor == "Y" ? true : false, null,
          adoptidn, lostidn, foundidn);
      }
      //--------------------//
      isSame = true;
      /// AllowSelectBreeds
      isSame = compareDefaults(defaultTable.Rows.Cast<DataRow>().Where(id => id.Field<string>("identifier") == "FooterHTML").FirstOrDefault(),
        optional.DisplayFooterHTML == "Y" ? true : false,
        optional.TextFooterHTML);
      if (!isSame)
      {
        LoadConfigList(configList.ConfigRows, "FooterHTML", optional.DisplayFooterHTML == "Y" ? true : false, optional.TextFooterHTML,
          adoptidn, lostidn, foundidn);
      }
      //--------------------//
      isSame = true;
      /// AllowLostReport
      isSame = compareDefaults(defaultTable.Rows.Cast<DataRow>().Where(id => id.Field<string>("identifier") == "AllowLostReport").FirstOrDefault(),
        optional.AllowLostReport == "Y" ? true : false,
        null);
      if (!isSame)
      {
        LoadConfigList(configList.ConfigRows, "AllowLostReport", optional.AllowLostReport == "Y" ? true : false, null,
          adoptidn, lostidn, foundidn);
      }
      //--------------------//
      isSame = true;
      /// AllowFoundReport
      isSame = compareDefaults(defaultTable.Rows.Cast<DataRow>().Where(id => id.Field<string>("identifier") == "AllowFoundReport").FirstOrDefault(),
        optional.AllowFoundReport == "Y" ? true : false,
        null);
      if (!isSame)
      {
        LoadConfigList(configList.ConfigRows, "AllowFoundReport", optional.AllowFoundReport == "Y" ? true : false, null,
          adoptidn, lostidn, foundidn);
      }
      //--------------------//
      isSame = true;
      /// AllowAdoptReport
      isSame = compareDefaults(defaultTable.Rows.Cast<DataRow>().Where(id => id.Field<string>("identifier") == "AllowAdoptReport").FirstOrDefault(),
        optional.AllowAdoptReport == "Y" ? true : false,
        null);
      if (!isSame)
      {
        LoadConfigList(configList.ConfigRows, "AllowAdoptReport", optional.AllowAdoptReport == "Y" ? true : false, null,
          adoptidn, lostidn, foundidn);
      }
      //--------------------//
      isSame = true;
      /// ShelterTitle
      isSame = compareDefaults(defaultTable.Rows.Cast<DataRow>().Where(id => id.Field<string>("identifier") == "ShelterTitle").FirstOrDefault(),
        optional.DisplayShelterTitle == "Y" ? true : false,
        optional.ShelterTitleText);
      if (!isSame)
      {
        LoadConfigList(configList.ConfigRows, "ShelterTitle", optional.DisplayShelterTitle == "Y" ? true : false, optional.ShelterTitleText,
          adoptidn, lostidn, foundidn);
      }

      /// Missing ShelterTitleLink
      /// Navigation Bar
      /// AllowAdverts
      /// 
      var message2 = "";
      foreach (var item in configList.ConfigRows)
      {

        try
        {
          dbC.InsertPH3Config(item);
        }
        catch (Exception e)
        {
          message2 += e.Message;
          continue;
        }
      }
      if (!string.IsNullOrEmpty(message2))
      {
        return View("ConfigError", message2);
      }
      return View();
    }

    internal void LoadConfigList(List<PH3ConfigModel.PH3ConfigRow> list,
      string identifier, bool display, string CustomText,
      int adoptidn, int lostidn, int foundidn)
    {
      if (adoptidn != 0)
      {
        list.Add(new PH3ConfigModel.PH3ConfigRow
        {
          PH3ClientResultsViewidn = adoptidn,
          Identifier = identifier,
          Display = display,
          CustomText = CustomText
        });
      }
      if (lostidn != 0)
      {
        list.Add(new PH3ConfigModel.PH3ConfigRow
        {
          PH3ClientResultsViewidn = lostidn,
          Identifier = identifier,
          Display = display,
          CustomText = CustomText
        });
      }
      if (foundidn != 0)
      {
        list.Add(new PH3ConfigModel.PH3ConfigRow
        {
          PH3ClientResultsViewidn = foundidn,
          Identifier = identifier,
          Display = display,
          CustomText = CustomText
        });
      }
    }


    internal bool compareDefaults(DataRow defaultRow, bool configDisplay, string configText)
    {
      var same = true;
      if (Convert.ToBoolean($"{defaultRow["display"]}") != configDisplay)
      {
        same = false;
      }
      if (string.IsNullOrEmpty(configText))
      {
        configText = $"{defaultRow["customText"]}"?.ToString();
      }
      if ($"{defaultRow["customText"]}"?.ToString() != configText)
      {
        same = false;
      }

      return same;
    }



    /// <summary>
    /// Function to verify if a URLName is available or not.
    /// </summary>
    /// <param name="URLName">Intended name</param>
    /// <returns>Y/N</returns>
    public string CheckURLNameAvailability(string URLName)
    {
      SetupUNPW();
      var isAvailable = dbC.CheckURLNameAvailability(URLName);

      if (isAvailable.Rows.Count < 1)
      {
        return "Y";
      }
      else
      {
        return "N";
      }
    }


  }
}