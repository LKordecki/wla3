﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WLA3.Models;
using static WLA3.Models.PH3ConfigModel;

namespace WLA3.ViewModels
{
  public class PH3Config_ViewModel
  {
    public PH3ConfigRequired required { get; set; }
    public PH3ConfigOptional optional { get; set; }
  }
}