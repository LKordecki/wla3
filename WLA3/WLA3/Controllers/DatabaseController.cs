﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.Entity;
using System.Data.Entity.Core.EntityClient;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using WLA3.Models;
using WLA3.ViewModels;
using static WLA3.Models.CustomModels;
using static WLA3.ViewModels.WLA_ViewModel;
using System.IO;
using System.Security.Cryptography;
using System.IO.Compression;


namespace WLA3.Controllers
{
	public class DatabaseController : Controller
	{
		WebAnimalEntities wa = new WebAnimalEntities();
		AnimalEntities animal = new AnimalEntities();

		internal static string ClientId = "";
		internal static string UserName = "";
		internal static string Password = "";


		internal void SetUNPW(string db, string username, string password, string clientId)
		{
			UserName = username;
			Password = password;
			ClientId = clientId.ToUpper();
			if (db == "WA")
			{
				ConnectionTools.ChangeDatabase(wa, userId: UserName, password: Password);
			}
			else if (db == "Animal")
			{
				ConnectionTools.ChangeDatabase(animal, userId: UserName, password: Password);
			}

		}



		internal ClientLookupModel GetUNPW()
		{
			var CLM = new ClientLookupModel();
			CLM.User = UserName;
			CLM.Password = Password;
			CLM.ClientId = ClientId;
			return CLM;
		}

		#region WLA Functions
		internal JurisdictionConfiguration GetTemplateJurisdiction(string clientId, int jurisdictionId)
		{
			ConnectionTools.ChangeDatabase(wa, userId: UserName, password: Password);

			var jurisdictionTemplate = (from j in wa.jurisdictions
																	where j.jurisdiction_id == jurisdictionId
																	&& j.client_id == clientId
																	select new JurisdictionConfiguration
																	{
																		ClientId = clientId,
																		Jurisdiction = j.jurisdiction1,
																		JurisdictionTitle = j.jurisdiction_title,
																		JurisdictionDescription = j.jurisdiction_desc,
																		Zipcodes = j.zipcodes,
																		OutOfArea = j.out_of_area ?? false,
																		OutOfAreaURL = j.out_of_area_url
																	}).FirstOrDefault();

			return jurisdictionTemplate;
		}

		internal void InsertJurisdictionPrice(JurisdictionPriceConfiguration item, int jurisdictionId)
		{
			ConnectionTools.ChangeDatabase(wa, userId: UserName, password: Password);

			///Insert new Jurisdiction Row
			var sb = new StringBuilder();
			sb.Append($"insert into [WebAnimal].[sysadm].[jurisdiction_price] (");
			sb.Append($"client_id, ");
			sb.Append($"jurisdiction_id, ");
			sb.Append($"processing_type, ");
			sb.Append($"code, ");
			sb.Append($"price_or_percent, ");
			sb.Append($"price, ");
			sb.Append($"price_min, ");
			sb.Append($"price_max, ");
			sb.Append($"price_percent, ");
			sb.Append($"tag_term, ");
			sb.Append($"tag_term_avail, ");
			sb.Append($"juvenile_tag, ");
			sb.Append($"description, ");
			sb.Append($"disclaimer, ");
			sb.Append($"availability_renew, ");
			sb.Append($"availability_new, ");
			//sb.Append($"date_interval, ");
			sb.Append($"per_trans_or_item, ");
			sb.Append($"date_effective, ");
			sb.Append($"date_expires, ");
			sb.Append($"display_jursidiction, ");
			sb.Append($"display_licenseselect, ");
			sb.Append($"sort_order ");
			sb.Append($") values (");
			sb.Append($"@{nameof(ClientId)},");
			sb.Append($"@{nameof(jurisdictionId)}, ");
			sb.Append($"@{nameof(item.ProcessingType)},");
			sb.Append($"@{nameof(item.Code)},");
			sb.Append($"@{nameof(item.PriceOrPercent)},");
			sb.Append($"@{nameof(item.Price)},");
			sb.Append($"@{nameof(item.PriceMin)},");
			sb.Append($"@{nameof(item.PriceMax)}, ");
			sb.Append($"@{nameof(item.PricePercent)},");
			sb.Append($"@{nameof(item.TagTerm)},");
			sb.Append($"@{nameof(item.TagTermAvailable)},");
			sb.Append($"@{nameof(item.JuvenileTag)},");
			sb.Append($"@{nameof(item.Description)},");
			sb.Append($"@{nameof(item.Disclaimer)}, ");
			sb.Append($"@{nameof(item.AvailabilityRenew)},");
			sb.Append($"@{nameof(item.AvailabilityNew)},");
			sb.Append($"@{nameof(item.PerTransOrItem)},");
			sb.Append($"@{nameof(item.DateEffective)},");
			sb.Append($"@{nameof(item.DateExpires)},");
			sb.Append($" 'Y' ,");
			sb.Append($" 'Y' ,");
			sb.Append($"1000");
			sb.Append($"); select scope_identity()");

			try
			{
				//var identifierRowId =
				wa.Database.SqlQuery<decimal>(sb.ToString(),
						new SqlParameter($"{nameof(ClientId)}", ClientId.ToUpper()),
						new SqlParameter($"{nameof(jurisdictionId)}", jurisdictionId),
						new SqlParameter($"{nameof(item.ProcessingType)}", item.ProcessingType ?? (object)DBNull.Value),
						new SqlParameter($"{nameof(item.Code)}", item.Code.ToUpper() ?? (object)DBNull.Value),
						new SqlParameter($"{nameof(item.PriceOrPercent)}", item.PriceOrPercent ?? (object)DBNull.Value),
						new SqlParameter($"{nameof(item.Price)}", item.Price ?? (object)DBNull.Value),
						new SqlParameter($"{nameof(item.PriceMin)}", item.PriceMin ?? (object)DBNull.Value),
						new SqlParameter($"{nameof(item.PriceMax)}", item.PriceMax ?? (object)DBNull.Value),
						new SqlParameter($"{nameof(item.PricePercent)}", item.PricePercent ?? (object)DBNull.Value),
						new SqlParameter($"{nameof(item.TagTerm)}", item.TagTerm ?? (object)DBNull.Value),
						new SqlParameter($"{nameof(item.TagTermAvailable)}", item.TagTermAvailable ?? (object)DBNull.Value),
						new SqlParameter($"{nameof(item.JuvenileTag)}", item.JuvenileTag),
						new SqlParameter($"{nameof(item.Description)}", item.Description ?? (object)DBNull.Value),
						new SqlParameter($"{nameof(item.Disclaimer)}", item.Disclaimer ?? (object)DBNull.Value),
						new SqlParameter($"{nameof(item.AvailabilityRenew)}", item.AvailabilityRenew ?? (object)DBNull.Value),
						new SqlParameter($"{nameof(item.AvailabilityNew)}", item.AvailabilityNew ?? (object)DBNull.Value),
						new SqlParameter($"{nameof(item.PerTransOrItem)}", item.PerTransOrItem ?? (object)DBNull.Value),
						new SqlParameter($"{nameof(item.DateEffective)}", item.DateEffective ?? (object)DBNull.Value),
						new SqlParameter($"{nameof(item.DateExpires)}", item.DateExpires ?? (object)DBNull.Value)
						//new SqlParameter($"{nameof(item.DisplayJurisdiction)}", item.DisplayJurisdiction.ToUpper()),
						//new SqlParameter($"{nameof(item.DisplayLicenseSelect)}", item.DisplayLicenseSelect.ToUpper())
						).FirstOrDefault();

				//return Convert.ToInt32(identifierRowId);
			}
			catch (Exception e)
			{
				var exception = e;
				throw;
			}
		}



		internal List<LockboxConfiguration> getLockboxValidations(string clientId)
		{
			ConnectionTools.ChangeDatabase(wa, userId: UserName, password: Password);

			var validInfo = new DataTable();
			var validCategory = clientId + "-LB-%";

			var sb = new StringBuilder();
			sb.Append(" select [web_client_validation_id] ");
			sb.Append(" ,[web_client_id] ");
			sb.Append(" ,[category] ");
			sb.Append(" ,[identifier] ");
			sb.Append(" ,[text] ");
			sb.Append(" FROM [WebAnimal].[sysadm].[web_client_validation] a ");
			sb.Append(" where category like ");
			sb.Append($"@{nameof(validCategory)}");
			sb.Append(" order by( select ");
			sb.Append(" count(b.identifier) ");
			sb.Append(" from [WebAnimal].[sysadm].[web_client_validation] b ");
			sb.Append(" where b.identifier = a.identifier ");
			sb.Append(" ) desc, identifier ");

			var sql = sb.ToString();
			var connection = new SqlConnection(wa.Database.Connection.ConnectionString);
			using (var command = new SqlCommand(sql, connection))
			{
				command.Parameters.AddWithValue($"{nameof(validCategory)}", validCategory);
				connection.Open();
				using (var reader = command.ExecuteReader())
				{
					validInfo.Load(reader);
				}
			}

			var lbValid = new List<LockboxConfiguration>();
			foreach (DataRow row in validInfo.Rows)
			{
				int? webClientId = null;
				if ($"{row["web_client_id"]}" != null) webClientId = Convert.ToInt32($"{row["web_client_id"]}");
				lbValid.Add(new LockboxConfiguration
				{
					ClientId = clientId.ToUpper(),
					WebClientValidationId = Convert.ToInt32($"{row["web_client_validation_id"]}"),
					WebClientId = webClientId,
					Category = $"{row["category"]}"?.ToString(),
					Identifier = $"{row["identifier"]}"?.ToString(),
					Text = $"{row["text"]}"?.ToString(),
				});
			}

			return lbValid;
		}

		internal JurisdictionConfiguration GetUpdateJurisdictionInformation(string clientId, int jurisdictionId)
		{
			ConnectionTools.ChangeDatabase(wa, userId: UserName, password: Password);

			var updateJurisdiction = (from j in wa.jurisdictions
																where j.jurisdiction_id == jurisdictionId
																&& j.client_id == clientId
																select new JurisdictionConfiguration
																{
																	ClientId = clientId,
																	Jurisdiction = j.jurisdiction1,
																	JurisdictionTitle = j.jurisdiction_title,
																	JurisdictionDescription = j.jurisdiction_desc,
																	Zipcodes = j.zipcodes,
																	OutOfArea = j.out_of_area ?? false,
																	OutOfAreaURL = j.out_of_area_url
																}).FirstOrDefault();

			return updateJurisdiction;
		}

		internal List<JurisdictionPriceConfiguration> GetJurisdictionPriceRecords(string clientId, int jurisdictionId)
		{
			ConnectionTools.ChangeDatabase(wa, userId: UserName, password: Password);

			var jurisdictionPriceRecords = (from p in wa.jurisdiction_price
																			where p.jurisdiction_id == jurisdictionId
																			&& p.client_id == clientId
																			select new JurisdictionPriceConfiguration
																			{
																				ClientId = p.client_id,
																				JurisdictionId = p.jurisdiction_id,
																				InUse = true,
																				IsUpdate = false,
																				JurisdictionPriceId = p.jurisdiction_price_id,
																				ProcessingType = p.processing_type,
																				Code = p.code,
																				PriceOrPercent = p.price_or_percent,
																				Price = p.price,
																				PriceMin = p.price_min,
																				PriceMax = p.price_max,
																				PricePercent = p.price_percent,
																				TagTerm = p.tag_term,
																				TagTermAvailable = p.tag_term_avail,
																				JuvenileTag = p.juvenile_tag,
																				Description = p.description,
																				Disclaimer = p.disclaimer,
																				AvailabilityNew = p.availability_new,
																				AvailabilityRenew = p.availability_renew,
																				PerTransOrItem = p.per_trans_or_item,
																				DateEffective = p.date_effective,
																				DateExpires = p.date_expires,
																				DisplayJurisdiction = p.display_jursidiction,
																				DisplayLicenseSelect = p.display_licenseselect,
																				SortOrder = p.sort_order
																			}).ToList();

			return jurisdictionPriceRecords;
		}

		internal int InsertJurisdiction(JurisdictionConfiguration jConfig)
		{
			ConnectionTools.ChangeDatabase(wa, userId: UserName, password: Password);

			///Insert new Jurisdiction Row
			var sb = new StringBuilder();
			sb.Append($"insert into [WebAnimal].[sysadm].[jurisdiction] (");
			sb.Append($"client_id, ");
			sb.Append($"jurisdiction_id_client, ");
			sb.Append($"jurisdiction, ");
			sb.Append($"jurisdiction_title, ");
			sb.Append($"jurisdiction_desc, ");
			sb.Append($"zipcodes, ");
			sb.Append($"out_of_area, ");
			sb.Append($"out_of_area_url ");
			sb.Append($") values (");
			sb.Append($"@{nameof(jConfig.ClientId)},");
			sb.Append($"1, ");
			sb.Append($"@{nameof(jConfig.Jurisdiction)},");
			sb.Append($"@{nameof(jConfig.JurisdictionTitle)},");
			sb.Append($"@{nameof(jConfig.JurisdictionDescription)},");
			sb.Append($"@{nameof(jConfig.Zipcodes)},");
			sb.Append($"@{nameof(jConfig.OutOfArea)},");
			sb.Append($"@{nameof(jConfig.OutOfAreaURL)}");
			sb.Append($"); select scope_identity()");

			try
			{
				var identifierRowId =
					wa.Database.SqlQuery<decimal>(sb.ToString(),
							new SqlParameter($"{nameof(jConfig.ClientId)}", jConfig.ClientId),
							new SqlParameter($"{nameof(jConfig.Jurisdiction)}", jConfig.Jurisdiction.ToUpper() ?? (object)DBNull.Value),
							new SqlParameter($"{nameof(jConfig.JurisdictionTitle)}", jConfig.JurisdictionTitle ?? (object)DBNull.Value),
							new SqlParameter($"{nameof(jConfig.JurisdictionDescription)}", jConfig.JurisdictionDescription ?? (object)DBNull.Value),
							new SqlParameter($"{nameof(jConfig.Zipcodes)}", jConfig.Zipcodes ?? ""),
							new SqlParameter($"{nameof(jConfig.OutOfArea)}", jConfig.OutOfArea),
							new SqlParameter($"{nameof(jConfig.OutOfAreaURL)}", jConfig.OutOfAreaURL ?? (object)DBNull.Value)
							).FirstOrDefault();

				return Convert.ToInt32(identifierRowId);
			}
			catch (Exception e)
			{
				var exception = e;
				throw;
			}
		}

		internal LockboxConfiguration getIndividualLockboxRecord(int validationId)
		{
			ConnectionTools.ChangeDatabase(wa, userId: UserName, password: Password);

			var validInfo = new DataTable();

			var sb = new StringBuilder();
			sb.Append(" select [web_client_validation_id] ");
			sb.Append(" ,[web_client_id] ");
			sb.Append(" ,[category] ");
			sb.Append(" ,[identifier] ");
			sb.Append(" ,[text] ");
			sb.Append(" FROM [WebAnimal].[sysadm].[web_client_validation] a ");
			sb.Append(" where web_client_validation_id = ");
			sb.Append($"@{nameof(validationId)}");

			var sql = sb.ToString();
			var connection = new SqlConnection(wa.Database.Connection.ConnectionString);
			using (var command = new SqlCommand(sql, connection))
			{
				command.Parameters.AddWithValue($"{nameof(validationId)}", validationId);
				connection.Open();
				using (var reader = command.ExecuteReader())
				{
					validInfo.Load(reader);
				}
			}

			var lbValid = new LockboxConfiguration();
			if (validInfo.Rows.Count > 0)
			{
				lbValid.WebClientValidationId = Convert.ToInt32(validInfo.Rows[0]["web_client_validation_id"]);
				lbValid.WebClientId = validInfo.Rows[0]["web_client_id"] == null ? (int?)null : Convert.ToInt32(validInfo.Rows[0]["web_client_id"]);
				lbValid.Category = validInfo.Rows[0]["category"].ToString();
				lbValid.Identifier = validInfo.Rows[0]["identifier"].ToString();
				lbValid.Text = validInfo.Rows[0]["text"].ToString();
			}

			return lbValid;
		}

		internal void changeAllLockboxValidation(int validationId, string textValue)
		{
			ConnectionTools.ChangeDatabase(wa, userId: UserName, password: Password);

			//Find the record
			var record = getIndividualLockboxRecord(validationId);
			if (record != null && record.WebClientId != null)
			{
				var clientWebId = record.WebClientId;
				var identifier = record.Identifier;

				// Set ALL the records found to the same text
				var sb = new StringBuilder();
				sb.Append($"update [WebAnimal].[sysadm].[web_client_validation] set ");
				sb.Append($" text = @{nameof(textValue)} ");
				sb.Append($" where web_client_id = @{nameof(clientWebId)}");
				sb.Append($" and identifier = @{nameof(identifier)}");

				try
				{
					wa.Database.ExecuteSqlCommand(sb.ToString(),
							new SqlParameter($"{nameof(textValue)}", textValue),
							new SqlParameter($"{nameof(clientWebId)}", clientWebId ?? (object)DBNull.Value),
							new SqlParameter($"{nameof(identifier)}", identifier)
							);
				}
				catch (Exception e)
				{
					var exception = e;
				}
			}
		}

		internal void changeOneLockboxValidation(int validationId, string textValue)
		{
			ConnectionTools.ChangeDatabase(wa, userId: UserName, password: Password);

			if (validationId != 0)
			{
				// Set ONE record to the text
				var sb = new StringBuilder();
				sb.Append($"update [WebAnimal].[sysadm].[web_client_validation] set ");
				sb.Append($" text = @{nameof(textValue)} ");
				sb.Append($" where [web_client_validation_id] = @{nameof(validationId)}");

				try
				{
					wa.Database.ExecuteSqlCommand(sb.ToString(),
							new SqlParameter($"{nameof(textValue)}", textValue),
							new SqlParameter($"{nameof(validationId)}", validationId)
							);
				}
				catch (Exception e)
				{
					var exception = e;
				}
			}
		}

		internal List<IdentifierLookupList> getIdentifierList(WLAVM wlavm)
		{
			ConnectionTools.ChangeDatabase(wa, userId: UserName, password: Password);
			var lists = (from j in wa.jurisdictions
									 where j.client_id == wlavm.ClientLookupModel.ClientId
									 //&& j.out_of_area == false
									 orderby j.out_of_area, j.jurisdiction_id
									 select new IdentifierLookupList
									 {
										 JurisdictionId = j.jurisdiction_id,
										 ClientId = j.client_id,
										 JurisdictionName = j.jurisdiction_title,
										 JurisdictionZipcodes = j.zipcodes,
										 OutOfArea = j.out_of_area
									 }).ToList();
			return lists;
		}

		internal ClientConfiguration getSingleIdentifier(EditIndividualIdentifierLookup lookup)
		{
			ConnectionTools.ChangeDatabase(wa, userId: UserName, password: Password);
			var identifier = (from i in wa.web_client_configuration
												where i.web_client_configuration_id == lookup.WebClientConfigurationId
												&& (i.web_client_id == lookup.WebClientId || i.web_client_id == 0)
												select new ClientConfiguration
												{
													WebClientConfigurationId = i.web_client_configuration_id,
													ConfigType = i.config_type,
													WebClientId = i.web_client_id,
													JurisdictionId = i.jurisdiction_id,
													Category = i.category,
													Identifier = i.identifier,
													DisplayInView = i.DisplayInView,
													RequiredField = i.RequiredField,
													ReturnType = i.ReturnType,
													CustomText = i.CustomText,
													AllowChange = i.AllowChange,
													PlaceholderText = i.PlaceholderText,
													Regex = i.Regex,
													ValidationError = i.Validation_Error,
													SortOrder = i.SortOrder,
													MaxLength = i.max_length,
													HelpHTML = i.helpHTML,
													ClientId = lookup.ClientId
												}).FirstOrDefault();

			identifier.Description = (from d in wa.web_client_identifier
																where d.identifier == identifier.Identifier
																select d.description).FirstOrDefault().ToString();


			return identifier;
		}

		internal bool checkPlaceholderExistance(int placeholderId)
		{
			ConnectionTools.ChangeDatabase(wa, userId: UserName, password: Password);
			var exists = (from e in wa.web_client_configuration_placeholdertext
										where e.web_client_configuration_placeholdertext_id == placeholderId
										select e).Any();
			return exists;
		}

		internal web_client_configuration checkDefaultIdentifier(EditIndividualIdentifierLookup lookup)
		{
			ConnectionTools.ChangeDatabase(wa, userId: UserName, password: Password);
			var identifierFromDB = (from i in wa.web_client_configuration
															where i.web_client_configuration_id == lookup.WebClientConfigurationId
															select i).FirstOrDefault();
			return identifierFromDB;
		}

		internal IdentifierVM SetIdentifierList(IdentifierListParams identifierListParams)
		{
			ConnectionTools.ChangeDatabase(wa, userId: UserName, password: Password);
			var IVM = new IdentifierVM();
			IVM.ConfigResults = new List<f_wl_config_Result>();


			//get counts of all identifiers
			var identifierCounts = (from counts in wa.web_client_configuration
															group counts by counts.identifier into c
															select new IdentifierCount
															{
																Identifier = c.Key,
																Count = c.Count()
															}
															).AsEnumerable();

			IVM.ConfigResults = (from c in wa.f_wl_config(identifierListParams.ClientId, identifierListParams.JurisdictionId.ToString())
														 //where c.identifier.Contains("%")
													 orderby identifierCounts.Where(ic => c.identifier == ic.Identifier).Select(ic => ic.Count).FirstOrDefault() descending
													 select c).ToList();

			//IVM.ConfigResults = IVM.ConfigResults.OrderByDescending(identifierCounts.Where(ic => IVM.ConfigResults.All(c => c.identifier == ic.Identifier)).Select(ic => ic.Count)).ToList();

			//.OrderByDescending( c => c.identifier == identifierCounts.Where(ic => c.Identifier))


			//IVM.ConfigResults = IVM.ConfigResults.Where(i => i.identifier == identifierCounts.).OrderBy(identifierCounts.Count).ToList();
			//IVM.ConfigResults = IVM.ConfigResults.Any(i => identifierCounts.Any(c => i.identifier == c.Identifier)).OrderBy(c => c.count).toList();

			IVM.ValidResults = new List<f_wl_config_valid_Result>();
			IVM.ValidResults = (from c in wa.f_wl_config_valid(identifierListParams.ClientId, identifierListParams.JurisdictionId.ToString())
														//where c.identifier.Contains("%")
													select c).ToList();

			IVM.ClientId = (from c in IVM.ConfigResults
											select c.client_id).FirstOrDefault();

			IVM.JurisdictionId = (from c in IVM.ConfigResults
														select c.jurisdiction_id).FirstOrDefault();
			/// Webclientid for 'DEFAULT' is entered as 0
			if (IVM.ClientId.ToUpper() == "DEFAULT")
			{
				IVM.WebClientId = 0;
			}
			else
			{
				IVM.WebClientId = (from c in IVM.ConfigResults
													 select c.web_client_id).FirstOrDefault();
			}

			foreach (var item in IVM.ConfigResults)
			{
				if (item.CustomText != null)
					item.CustomText = item.CustomText.Replace(",", ", ").Replace(" ,  ", ", ");
				if (item.PlaceholderText != null)
					item.PlaceholderText = item.PlaceholderText.Replace(",", ", ").Replace(" ,  ", ", ");
			}

			return IVM;
		}

		internal List<PlaceholderText> setPlaceholderList(int webClientConfigurationId)
		{
			ConnectionTools.ChangeDatabase(wa, userId: UserName, password: Password);
			var list = (from p in wa.web_client_configuration_placeholdertext
									where p.web_client_configuration_id == webClientConfigurationId
									select new PlaceholderText
									{
										WebClientConfigurationId = p.web_client_configuration_id,
										WebClientConfigPlaceholderId = p.web_client_configuration_placeholdertext_id,
										Key = p.Key,
										Value = p.Value,
										SortOrder = p.SortOrder,
										InUse = true
									}).ToList();
			return list;
		}

		internal List<JurisdictionConfiguration> getJurisdictionInfo(string clientId)
		{
			ConnectionTools.ChangeDatabase(wa, userId: UserName, password: Password);
			var jInfo = (from j in wa.jurisdictions
									 where j.client_id == clientId
									 select new JurisdictionConfiguration
									 {
										 ClientId = clientId,
										 JurisdictionId = j.jurisdiction_id,
										 Jurisdiction = j.jurisdiction1,
										 JurisdictionTitle = j.jurisdiction_title,
										 JurisdictionDescription = j.jurisdiction_desc,
										 Zipcodes = j.zipcodes,
										 OutOfArea = j.out_of_area ?? false,
										 OutOfAreaURL = j.out_of_area_url,
										 JurisdictionPrice = (from p in wa.jurisdiction_price
																					where p.jurisdiction_id == j.jurisdiction_id
																					select new JurisdictionPriceConfiguration
																					{
																						InUse = true,
																						JurisdictionPriceId = p.jurisdiction_price_id,
																						ProcessingType = p.processing_type,
																						Code = p.code,
																						PriceOrPercent = p.price_or_percent,
																						Price = p.price,
																						PriceMin = p.price_min,
																						PriceMax = p.price_max,
																						PricePercent = p.price_percent,
																						TagTerm = p.tag_term,
																						TagTermAvailable = p.tag_term_avail,
																						JuvenileTag = p.juvenile_tag,
																						Description = p.description,
																						Disclaimer = p.disclaimer,
																						AvailabilityNew = p.availability_new,
																						AvailabilityRenew = p.availability_renew,
																						PerTransOrItem = p.per_trans_or_item,
																						DateEffective = p.date_effective,
																						DateExpires = p.date_expires,
																						DisplayJurisdiction = p.display_jursidiction,
																						DisplayLicenseSelect = p.display_licenseselect
																					}).ToList()
									 }).ToList();

			return jInfo;
		}

		internal void updateJuridictionInfo(JurisdictionConfiguration jConfig)
		{
			ConnectionTools.ChangeDatabase(wa, userId: UserName, password: Password);

			if (jConfig.Zipcodes == null)
			{
				jConfig.Zipcodes = " ";
			}

			var sb = new StringBuilder();
			sb.Append($"update [WebAnimal].[sysadm].[jurisdiction] set ");
			sb.Append($"jurisdiction = @{nameof(jConfig.Jurisdiction)}, ");
			sb.Append($"jurisdiction_title = @{nameof(jConfig.JurisdictionTitle)}, ");
			sb.Append($"jurisdiction_desc = @{nameof(jConfig.JurisdictionDescription)}, ");
			sb.Append($"zipcodes = @{nameof(jConfig.Zipcodes)}, ");
			sb.Append($"out_of_area = @{nameof(jConfig.OutOfArea)}, ");
			sb.Append($"out_of_area_url = @{nameof(jConfig.OutOfAreaURL)} ");
			sb.Append($" where jurisdiction_id = @{nameof(jConfig.JurisdictionId)}");

			try
			{
				wa.Database.ExecuteSqlCommand(sb.ToString(),
						new SqlParameter($"{nameof(jConfig.Jurisdiction)}", jConfig.Jurisdiction ?? (object)DBNull.Value),
						new SqlParameter($"{nameof(jConfig.JurisdictionTitle)}", jConfig.JurisdictionTitle ?? (object)DBNull.Value),
						new SqlParameter($"{nameof(jConfig.JurisdictionDescription)}", jConfig.JurisdictionDescription ?? (object)DBNull.Value),
						new SqlParameter($"{nameof(jConfig.Zipcodes)}", jConfig.Zipcodes),
						new SqlParameter($"{nameof(jConfig.OutOfArea)}", jConfig.OutOfArea),
						new SqlParameter($"{nameof(jConfig.OutOfAreaURL)}", jConfig.OutOfAreaURL ?? (object)DBNull.Value),
						new SqlParameter($"{nameof(jConfig.JurisdictionId)}", jConfig.JurisdictionId)
						);
			}
			catch (Exception e)
			{
				var exception = e;
			}
		}

		internal int insertIdentifier(ClientConfiguration row)
		{
			ConnectionTools.ChangeDatabase(wa, userId: UserName, password: Password);
			///if it was a default, we want to create a new row in the DB with the info provided
			///First we insert the row
			///
			var sb = new StringBuilder();
			sb.Append($"insert into [WebAnimal].[sysadm].[web_client_configuration] (");
			sb.Append($"config_type, ");
			sb.Append($"web_client_id, ");
			sb.Append($"jurisdiction_id, ");
			sb.Append($"category, ");
			sb.Append($"identifier, ");
			sb.Append($"DisplayInView, ");
			sb.Append($"RequiredField, ");
			sb.Append($"AllowChange, ");
			sb.Append($"ReturnType, ");
			sb.Append($"CustomText, ");
			sb.Append($"PlaceholderText, ");
			sb.Append($"Regex, ");
			sb.Append($"Validation_Error, ");
			sb.Append($"SortOrder, ");
			sb.Append($"max_length, ");
			sb.Append($"helpHTML");
			sb.Append($") values (");
			sb.Append($"@{nameof(row.ConfigType)},");
			sb.Append($"@{nameof(row.WebClientId)},");
			sb.Append($"@{nameof(row.JurisdictionId)},");
			sb.Append($"@{nameof(row.Category)},");
			sb.Append($"@{nameof(row.Identifier)},");
			sb.Append($"@{nameof(row.DisplayInView)},");
			sb.Append($"@{nameof(row.RequiredField)},");
			sb.Append($"@{nameof(row.AllowChange)},");
			sb.Append($"@{nameof(row.ReturnType)},");
			sb.Append($"@{nameof(row.CustomText)},");
			sb.Append($"@{nameof(row.PlaceholderText)},");
			sb.Append($"@{nameof(row.Regex)},");
			sb.Append($"@{nameof(row.ValidationError)},");
			sb.Append($"@{nameof(row.SortOrder)},");
			sb.Append($"@{nameof(row.MaxLength)},");
			sb.Append($"@{nameof(row.HelpHTML)}");
			sb.Append($"); select scope_identity()");

			try
			{
				var identifierRowId =
					wa.Database.SqlQuery<decimal>(sb.ToString(),
							new SqlParameter($"{nameof(row.ConfigType)}", row.ConfigType),
							new SqlParameter($"{nameof(row.WebClientId)}", row.WebClientId),
							new SqlParameter($"{nameof(row.JurisdictionId)}", row.JurisdictionId),
							new SqlParameter($"{nameof(row.Category)}", row.Category),
							new SqlParameter($"{nameof(row.Identifier)}", row.Identifier),
							new SqlParameter($"{nameof(row.DisplayInView)}", row.DisplayInView),
							new SqlParameter($"{nameof(row.RequiredField)}", row.RequiredField),
							new SqlParameter($"{nameof(row.AllowChange)}", row.AllowChange),
							new SqlParameter($"{nameof(row.ReturnType)}", row.ReturnType ?? (object)DBNull.Value),
							new SqlParameter($"{nameof(row.CustomText)}", row.CustomText ?? (object)DBNull.Value),
							new SqlParameter($"{nameof(row.PlaceholderText)}", row.PlaceholderText ?? (object)DBNull.Value),
							new SqlParameter($"{nameof(row.Regex)}", row.Regex ?? (object)DBNull.Value),
							new SqlParameter($"{nameof(row.ValidationError)}", row.ValidationError ?? (object)DBNull.Value),
							new SqlParameter($"{nameof(row.SortOrder)}", row.SortOrder ?? (object)DBNull.Value),
							new SqlParameter($"{nameof(row.MaxLength)}", row.MaxLength ?? (object)DBNull.Value),
							new SqlParameter($"{nameof(row.HelpHTML)}", row.HelpHTML ?? (object)DBNull.Value)
							).FirstOrDefault();

				return Convert.ToInt32(identifierRowId);
			}
			catch (Exception e)
			{
				var exception = e;
				throw;
			}
		}

		internal void updateIdentifier(ClientConfiguration row)
		{
			ConnectionTools.ChangeDatabase(wa, userId: UserName, password: Password);
			var sb = new StringBuilder();
			sb.Append($"update [WebAnimal].[sysadm].[web_client_configuration] set ");
			sb.Append($"category = @{nameof(row.Category)}, ");
			sb.Append($"DisplayInView = @{nameof(row.DisplayInView)}, ");
			sb.Append($"RequiredField = @{nameof(row.RequiredField)}, ");
			sb.Append($"AllowChange = @{nameof(row.AllowChange)}, ");
			sb.Append($"ReturnType = @{nameof(row.ReturnType)}, ");
			sb.Append($"CustomText = @{nameof(row.CustomText)}, ");
			sb.Append($"PlaceholderText = @{nameof(row.PlaceholderText)}, ");
			sb.Append($"Regex = @{nameof(row.Regex)}, ");
			sb.Append($"Validation_Error = @{nameof(row.ValidationError)}, ");
			sb.Append($"SortOrder = @{nameof(row.SortOrder)}, ");
			sb.Append($"max_length = @{nameof(row.MaxLength)},");
			sb.Append($"helpHtml = @{nameof(row.HelpHTML)}");
			sb.Append($" where web_client_configuration_id = @{nameof(row.WebClientConfigurationId)}");

			try
			{
				var identifierRowId =
					wa.Database.ExecuteSqlCommand(sb.ToString(),
							new SqlParameter($"{nameof(row.ConfigType)}", row.ConfigType),
							new SqlParameter($"{nameof(row.WebClientId)}", row.WebClientId),
							new SqlParameter($"{nameof(row.JurisdictionId)}", row.JurisdictionId),
							new SqlParameter($"{nameof(row.Category)}", row.Category),
							new SqlParameter($"{nameof(row.Identifier)}", row.Identifier),
							new SqlParameter($"{nameof(row.DisplayInView)}", row.DisplayInView),
							new SqlParameter($"{nameof(row.RequiredField)}", row.RequiredField),
							new SqlParameter($"{nameof(row.AllowChange)}", row.AllowChange),
							new SqlParameter($"{nameof(row.ReturnType)}", row.ReturnType ?? (object)DBNull.Value),
							new SqlParameter($"{nameof(row.CustomText)}", row.CustomText ?? (object)DBNull.Value),
							new SqlParameter($"{nameof(row.PlaceholderText)}", row.PlaceholderText ?? (object)DBNull.Value),
							new SqlParameter($"{nameof(row.Regex)}", row.Regex ?? (object)DBNull.Value),
							new SqlParameter($"{nameof(row.ValidationError)}", row.ValidationError ?? (object)DBNull.Value),
							new SqlParameter($"{nameof(row.SortOrder)}", row.SortOrder ?? (object)DBNull.Value),
							new SqlParameter($"{nameof(row.MaxLength)}", row.MaxLength ?? (object)DBNull.Value),
							new SqlParameter($"{nameof(row.HelpHTML)}", row.HelpHTML ?? (object)DBNull.Value),
							new SqlParameter($"{nameof(row.WebClientConfigurationId)}", row.WebClientConfigurationId)
							).ToString();
			}
			catch (Exception e)
			{
				var exception = e;
			}
		}

		internal void updatePlaceholderRow(PlaceholderText placeholder, int webClientConfigurationId)
		{
			ConnectionTools.ChangeDatabase(wa, userId: UserName, password: Password);
			var sb = new StringBuilder();
			sb.Append($"update [WebAnimal].[sysadm].[web_client_configuration_placeholdertext] set ");
			sb.Append($"web_client_configuration_id = @{nameof(webClientConfigurationId)}, ");
			sb.Append($"[Key] = @{nameof(placeholder.Key)}, ");
			sb.Append($"Value = @{nameof(placeholder.Value)}, ");
			sb.Append($"SortOrder = @{nameof(placeholder.SortOrder)}");
			sb.Append($" where web_client_configuration_placeholdertext_id = @{nameof(placeholder.WebClientConfigPlaceholderId)}");
			try
			{
				wa.Database.ExecuteSqlCommand(sb.ToString(),
						new SqlParameter($"{nameof(webClientConfigurationId)}", webClientConfigurationId),
						new SqlParameter($"{nameof(placeholder.Key)}", placeholder.Key),
						new SqlParameter($"{nameof(placeholder.Value)}", placeholder.Value),
						new SqlParameter($"{nameof(placeholder.SortOrder)}", placeholder.SortOrder ?? (object)DBNull.Value),
						new SqlParameter($"{nameof(placeholder.WebClientConfigPlaceholderId)}", placeholder.WebClientConfigPlaceholderId)
						);
			}
			catch (Exception e)
			{
				var exception = e;
			}
		}

		internal void insertPlaceholderRow(PlaceholderText placeholder, int rowId)
		{
			ConnectionTools.ChangeDatabase(wa, userId: UserName, password: Password);
			var sb = new StringBuilder();
			sb.Append($"insert into [WebAnimal].[sysadm].[web_client_configuration_placeholdertext] (");
			sb.Append($"web_client_configuration_id, ");
			sb.Append($"[Key], ");
			sb.Append($"Value, ");
			sb.Append($"SortOrder ");
			sb.Append($") values (");
			sb.Append($"@{nameof(rowId)}, ");
			sb.Append($"@{nameof(placeholder.Key)}, ");
			sb.Append($"@{nameof(placeholder.Value)}, ");
			sb.Append($"@{nameof(placeholder.SortOrder)} ");
			sb.Append($")");
			try
			{
				wa.Database.ExecuteSqlCommand(sb.ToString(),
						new SqlParameter($"{nameof(rowId)}", rowId),
						new SqlParameter($"{nameof(placeholder.Key)}", placeholder.Key),
						new SqlParameter($"{nameof(placeholder.Value)}", placeholder.Value),
						new SqlParameter($"{nameof(placeholder.SortOrder)}", placeholder.SortOrder ?? (object)DBNull.Value)
						);
			}
			catch (Exception e)
			{
				var exception = e;
			}
		}

		internal void removePlaceholder(int webClientConfigPlaceholderId)
		{
			ConnectionTools.ChangeDatabase(wa, userId: UserName, password: Password);
			var sb = new StringBuilder();
			sb.Append($"delete from [WebAnimal].[sysadm].[web_client_configuration_placeholdertext] ");
			sb.Append($" where web_client_configuration_placeholdertext_id = @{nameof(webClientConfigPlaceholderId)}");

			try
			{
				wa.Database.ExecuteSqlCommand(sb.ToString(),
						new SqlParameter($"{nameof(webClientConfigPlaceholderId)}", webClientConfigPlaceholderId)
						);
			}
			catch (Exception e)
			{
				var exception = e;
			}
		}

		internal void removeIdentifier(int identifierId)
		{
			ConnectionTools.ChangeDatabase(wa, userId: UserName, password: Password);
			var sb = new StringBuilder();
			sb.Append($"delete from [WebAnimal].[sysadm].[web_client_configuration] ");
			sb.Append($" where web_client_configuration_id = @{nameof(identifierId)}");

			try
			{
				wa.Database.ExecuteSqlCommand(sb.ToString(),
						new SqlParameter($"{nameof(identifierId)}", identifierId)
						);
			}
			catch (Exception e)
			{
				var exception = e;
			}
		}

		internal void updateJurisdictionPriceRecord(JurisdictionPriceConfiguration item)
		{
			ConnectionTools.ChangeDatabase(wa, userId: UserName, password: Password);
			var sb = new StringBuilder();
			sb.Append($"update [WebAnimal].[sysadm].[jurisdiction_price] set ");
			sb.Append($" processing_type = @{nameof(item.ProcessingType)}, ");
			sb.Append($" code = @{nameof(item.Code)}, ");
			sb.Append($" price = @{nameof(item.Price)}, ");
			sb.Append($" price_min = @{nameof(item.PriceMin)}, ");
			sb.Append($" price_max = @{nameof(item.PriceMax)}, ");
			sb.Append($" tag_term = @{nameof(item.TagTerm)}, ");
			sb.Append($" tag_term_avail = @{nameof(item.TagTermAvailable)}, ");
			sb.Append($" juvenile_tag = @{nameof(item.JuvenileTag)}, ");
			sb.Append($" description = @{nameof(item.Description)}, ");
			sb.Append($" disclaimer = @{nameof(item.Disclaimer)}, ");
			sb.Append($" availability_new = @{nameof(item.AvailabilityNew)}, ");
			sb.Append($" availability_renew = @{nameof(item.AvailabilityRenew)}, ");
			sb.Append($" per_trans_or_item = @{nameof(item.PerTransOrItem)}, ");
			sb.Append($" date_effective = @{nameof(item.DateEffective)}, ");
			sb.Append($" date_expires = @{nameof(item.DateExpires)}, ");
			sb.Append($" sort_order = @{nameof(item.SortOrder)} ");
			sb.Append($" where jurisdiction_price_id = @{nameof(item.JurisdictionPriceId)}");

			try
			{
				wa.Database.ExecuteSqlCommand(sb.ToString(),
						new SqlParameter($"{nameof(item.ProcessingType)}", item.ProcessingType ?? (object)DBNull.Value),
						new SqlParameter($"{nameof(item.Code)}", item.Code ?? (object)DBNull.Value),
						new SqlParameter($"{nameof(item.Price)}", item.Price ?? (object)DBNull.Value),
						new SqlParameter($"{nameof(item.PriceMin)}", item.PriceMin ?? (object)DBNull.Value),
						new SqlParameter($"{nameof(item.PriceMax)}", item.PriceMax ?? (object)DBNull.Value),
						new SqlParameter($"{nameof(item.TagTerm)}", item.TagTerm ?? (object)DBNull.Value),
						new SqlParameter($"{nameof(item.TagTermAvailable)}", item.TagTermAvailable ?? (object)DBNull.Value),
						new SqlParameter($"{nameof(item.JuvenileTag)}", item.JuvenileTag),
						new SqlParameter($"{nameof(item.Description)}", item.Description ?? (object)DBNull.Value),
						new SqlParameter($"{nameof(item.Disclaimer)}", item.Disclaimer ?? (object)DBNull.Value),
						new SqlParameter($"{nameof(item.AvailabilityNew)}", item.AvailabilityNew ?? (object)DBNull.Value),
						new SqlParameter($"{nameof(item.AvailabilityRenew)}", item.AvailabilityRenew ?? (object)DBNull.Value),
						new SqlParameter($"{nameof(item.PerTransOrItem)}", item.PerTransOrItem ?? (object)DBNull.Value),
						new SqlParameter($"{nameof(item.DateEffective)}", item.DateEffective ?? (object)DBNull.Value),
						new SqlParameter($"{nameof(item.DateExpires)}", item.DateExpires ?? (object)DBNull.Value),
						new SqlParameter($"{nameof(item.SortOrder)}", item.SortOrder),
						new SqlParameter($"{nameof(item.JurisdictionPriceId)}", item.JurisdictionPriceId)
						);
			}
			catch (Exception e)
			{
				var exception = e;
			}
		}

		internal void addNewJurisdictionPriceRecord(JurisdictionPriceConfiguration item)
		{
			ConnectionTools.ChangeDatabase(wa, userId: UserName, password: Password);
			var sb = new StringBuilder();
			sb.Append($"insert into [WebAnimal].[sysadm].[jurisdiction_price] ( ");
			sb.Append($" client_id, ");
			sb.Append($" jurisdiction_id, ");
			sb.Append($" processing_type, ");
			sb.Append($" code, ");
			sb.Append($" price, ");
			sb.Append($" price_min, ");
			sb.Append($" price_max, ");
			sb.Append($" tag_term, ");
			sb.Append($" tag_term_avail, ");
			sb.Append($" juvenile_tag, ");
			sb.Append($" [description], ");
			sb.Append($" disclaimer, ");
			sb.Append($" availability_new, ");
			sb.Append($" availability_renew, ");
			sb.Append($" per_trans_or_item, ");
			sb.Append($" date_effective, ");
			sb.Append($" date_expires, ");
			sb.Append($" display_jursidiction, ");
			sb.Append($" display_licenseselect, ");
			sb.Append($" sort_order  ");
			sb.Append($" ) values ( ");
			sb.Append($"@{nameof(item.ClientId)}, ");
			sb.Append($"@{nameof(item.JurisdictionId)}, ");
			sb.Append($"@{nameof(item.ProcessingType)}, ");
			sb.Append($"@{nameof(item.Code)}, ");
			sb.Append($"@{nameof(item.Price)}, ");
			sb.Append($"@{nameof(item.PriceMin)}, ");
			sb.Append($"@{nameof(item.PriceMax)}, ");
			sb.Append($"@{nameof(item.TagTerm)}, ");
			sb.Append($"@{nameof(item.TagTermAvailable)}, ");
			sb.Append($"@{nameof(item.JuvenileTag)}, ");
			sb.Append($"@{nameof(item.Description)}, ");
			sb.Append($"@{nameof(item.Disclaimer)}, ");
			sb.Append($"@{nameof(item.AvailabilityNew)}, ");
			sb.Append($"@{nameof(item.AvailabilityRenew)}, ");
			sb.Append($"@{nameof(item.PerTransOrItem)}, ");
			sb.Append($"@{nameof(item.DateEffective)}, ");
			sb.Append($"@{nameof(item.DateExpires)}, ");
			sb.Append($"'Y', ");
			sb.Append($"'Y', ");
			sb.Append($"@{nameof(item.SortOrder)} )");
			try
			{
				wa.Database.ExecuteSqlCommand(sb.ToString(),
						new SqlParameter($"{nameof(item.ClientId)}", item.ClientId ?? (object)DBNull.Value),
						new SqlParameter($"{nameof(item.JurisdictionId)}", item.JurisdictionId ?? (object)DBNull.Value),
						new SqlParameter($"{nameof(item.ProcessingType)}", item.ProcessingType ?? (object)DBNull.Value),
						new SqlParameter($"{nameof(item.Code)}", item.Code ?? (object)DBNull.Value),
						new SqlParameter($"{nameof(item.Price)}", item.Price ?? (object)DBNull.Value),
						new SqlParameter($"{nameof(item.PriceMin)}", item.PriceMin ?? (object)DBNull.Value),
						new SqlParameter($"{nameof(item.PriceMax)}", item.PriceMax ?? (object)DBNull.Value),
						new SqlParameter($"{nameof(item.TagTerm)}", item.TagTerm ?? (object)DBNull.Value),
						new SqlParameter($"{nameof(item.TagTermAvailable)}", item.TagTermAvailable ?? (object)DBNull.Value),
						new SqlParameter($"{nameof(item.JuvenileTag)}", item.JuvenileTag),
						new SqlParameter($"{nameof(item.Description)}", item.Description ?? (object)DBNull.Value),
						new SqlParameter($"{nameof(item.Disclaimer)}", item.Disclaimer ?? (object)DBNull.Value),
						new SqlParameter($"{nameof(item.AvailabilityNew)}", item.AvailabilityNew ?? (object)DBNull.Value),
						new SqlParameter($"{nameof(item.AvailabilityRenew)}", item.AvailabilityRenew ?? (object)DBNull.Value),
						new SqlParameter($"{nameof(item.PerTransOrItem)}", item.PerTransOrItem ?? (object)DBNull.Value),
						new SqlParameter($"{nameof(item.DateEffective)}", item.DateEffective ?? (object)DBNull.Value),
						new SqlParameter($"{nameof(item.DateExpires)}", item.DateExpires ?? (object)DBNull.Value),
						new SqlParameter($"{nameof(item.SortOrder)}", item.SortOrder)
						);
			}
			catch (Exception e)
			{
				var exception = e;
			}
		}

		internal void deleteJurisdictionPriceRecord(int jurisdictionPriceId)
		{
			ConnectionTools.ChangeDatabase(wa, userId: UserName, password: Password);
			var sb = new StringBuilder();
			sb.Append($"delete from [WebAnimal].[sysadm].[jurisdiction_price] ");
			sb.Append($" where jurisdiction_price_id = @{nameof(jurisdictionPriceId)}");

			try
			{
				wa.Database.ExecuteSqlCommand(sb.ToString(),
						new SqlParameter($"{nameof(jurisdictionPriceId)}", jurisdictionPriceId)
						);
			}
			catch (Exception e)
			{
				var exception = e;
			}
		}

		internal DataTable testSQL(string returnText, DataTable dt)
		{
			ConnectionTools.ChangeDatabase(wa, userId: UserName, password: Password);
			using (var conn = new SqlConnection(wa.Database.Connection.ConnectionString))
			using (var cmd = new SqlCommand(returnText, conn))
			{
				conn.Open();
				dt.Load(cmd.ExecuteReader());
			}
			return dt;
		}

		#endregion


		#region PH Images Functions
		/// Get Pending Image Count
		internal int getImageCount()
		{
			ConnectionTools.ChangeDatabase(animal, userId: UserName, password: Password);
			var pictureCount = new DataTable();
			var sb = new StringBuilder();
			sb.Append(" select ");
			sb.Append(" p.image_id, ");
			sb.Append(" p.image_seq, ");
			sb.Append(" p.image_data, ");
			sb.Append(" p.image_resolution, ");
			sb.Append(" p.image_type, ");
			sb.Append(" p.userid, ");
			sb.Append(" p.[location], ");
			sb.Append(" p.stamp, ");
			sb.Append(" p.uploaded, ");
			sb.Append(" p.delete_date, ");
			sb.Append(" p.userid as [located_at], ");
			sb.Append(" b.* ");
			//sb.Append(" b.email_address, ");
			//sb.Append(" b.interest_type, ");
			//sb.Append(" b.first_name, ");
			//sb.Append(" b.last_name, ");
			//sb.Append(" b.req_stat, ");
			//sb.Append(" b.breed_req_id, ");
			//sb.Append(" b.shelter_list, ");
			//sb.Append(" b.proper_name, ");
			//sb.Append(" b.animal_type ");
			sb.Append(" from ");
			sb.Append(" sysadm.breed_req b, sysadm.picture_it p  ");
			sb.Append(" WHERE p.userid = 'PUBLIC' and p.location = 'PetReq' and b.req_stat in ('pending', 'active') ");
			sb.Append(" and p.image_id = 'T' + cast(b.breed_req_id as varchar(10))  ");
			//sb.Append(" WHERE p.userid = 'PUBLIC' and p.location = 'PetReq' and p.image_id like 'T%' ");
			sb.Append(" AND image_resolution = 'DETAIL' ORDER BY image_id, image_seq ASC ");

			var sql = sb.ToString();
			var connection = new SqlConnection(animal.Database.Connection.ConnectionString);
			using (var command = new SqlCommand(sql, connection))
			{
				connection.Open();
				using (var reader = command.ExecuteReader())
				{
					pictureCount.Load(reader);
				}
			}
			return pictureCount.Rows.Count;
		}


		/// Get pending images
		internal List<ImageInformation> getPendingImages()
		{
			ConnectionTools.ChangeDatabase(animal, userId: UserName, password: Password);

			/// Get the list of images
			var pictureTable = new DataTable();
			var sb = new StringBuilder();

			sb.Append(" ;with picts as (select image_id, p.image_seq,  p.image_data,  p.image_resolution,   ");
			sb.Append(" p.image_type,  p.userid,  p.[location],  p.stamp,  p.uploaded,  p.delete_date,  p.userid as [located_at] ");
			sb.Append(" from sysadm.picture_it p  ");
			sb.Append(" WHERE p.userid = 'PUBLIC' and p.location = 'PetReq'  ");
			sb.Append(" and p.image_id like 'T%' AND image_resolution = 'DETAIL' ) ");

			sb.Append(" select *  ");
			sb.Append(" from sysadm.breed_req b  ");
			sb.Append(" join picts p on p.image_id = 'T' + b.breed_req_id_vc ");
			sb.Append(" where b.req_stat in ('pending', 'active') ");


			var sql = sb.ToString();
			var connection = new SqlConnection(animal.Database.Connection.ConnectionString);
			using (var command = new SqlCommand(sql, connection))
			{
				command.CommandTimeout = 240;
				connection.Open();
				using (var reader = command.ExecuteReader())
				{
					pictureTable.Load(reader);
				}
			}
			/// Load them into the model
			/// 
			if (pictureTable.Rows.Count >= 20)
			{
				pictureTable = pictureTable.Rows.Cast<System.Data.DataRow>().Take(20).CopyToDataTable();
			}

			return loadImageInformation(pictureTable);
		}



		internal List<ImageInformation> lookupBy(string ImageLookup)
		{
			ConnectionTools.ChangeDatabase(animal, userId: UserName, password: Password);

			var queryParam = "";
			var addWithValue = new List<UtilityModel.LookupArrray>();

			if (ImageLookup.Contains("@"))
			{
				// Probably and email address so query on email
				queryParam = $" b.email_address = @{nameof(ImageLookup)} ";
				addWithValue.Add(new UtilityModel.LookupArrray
				{
					lookupString = $"{nameof(ImageLookup)}",
					lookupObject = ImageLookup
				});
			}
			else if (!ImageLookup.Any(char.IsDigit))
			{
				if (ImageLookup.Contains(' '))
				{
					// probably a first last name
					var name = ImageLookup.Split(' ');
					var firstName = name[0];
					var lastName = name[1];
					queryParam = $" b.First_Name = @{nameof(firstName)} and b.Last_Name = @{nameof(lastName)} ";
					addWithValue.Add(new UtilityModel.LookupArrray
					{
						lookupString = $"{nameof(firstName)}",
						lookupObject = firstName
					});
					addWithValue.Add(new UtilityModel.LookupArrray
					{
						lookupString = $"{nameof(lastName)}",
						lookupObject = lastName
					});
				}
				else
				{
					// Probably a last name
					queryParam = $" b.Last_Name = @{nameof(ImageLookup)}";
					addWithValue.Add(new UtilityModel.LookupArrray
					{
						lookupString = $"{nameof(ImageLookup)}",
						lookupObject = ImageLookup
					});
				}

			}
			else if (ImageLookup.Length < 10)
			{
				// Probably a breed Request #
				queryParam = $" b.breed_req_id = @{nameof(ImageLookup)} ";
				addWithValue.Add(new UtilityModel.LookupArrray
				{
					lookupString = $"{nameof(ImageLookup)}",
					lookupObject = ImageLookup
				});
			}
			else if (ImageLookup.Length >= 10)
			{
				// Probably a phone number
				var strippedPhone = ImageLookup != null && ImageLookup.Length > 0 ? ImageLookup.Replace("-", "").Replace("(", "").Replace(")", "").Replace(" ", "") : string.Empty;
				if (ImageLookup.Length == 10)
				{
					var phone0Number = strippedPhone != null && strippedPhone.Length > 0 ? strippedPhone.Substring(3, 7) : string.Empty;
					ImageLookup = phone0Number;
					queryParam = $" b.phone_Number = @{nameof(ImageLookup)} ";
					addWithValue.Add(new UtilityModel.LookupArrray
					{
						lookupString = $"{nameof(ImageLookup)}",
						lookupObject = ImageLookup
					});
				}
				if (ImageLookup.Length == 11)
				{
					var phone0Number = strippedPhone != null && strippedPhone.Length > 0 ? strippedPhone.Substring(4, 8) : string.Empty;
					ImageLookup = phone0Number;
					queryParam = $" b.phone_Number = @{nameof(ImageLookup)} ";
					addWithValue.Add(new UtilityModel.LookupArrray
					{
						lookupString = $"{nameof(ImageLookup)}",
						lookupObject = ImageLookup
					});
				}
			}


			/// Get the list of images
			var pictureTable = new DataTable();
			var sb = new StringBuilder();
			sb.Append(" select ");
			sb.Append(" p.image_id ");
			sb.Append(" , b.* ");
			sb.Append(" , p.image_data ");
			sb.Append(" , p.image_seq ");
			sb.Append(" , p.image_resolution ");
			sb.Append(" , p.image_type ");
			sb.Append(" , p.userid ");
			sb.Append(" , p.[location] ");
			sb.Append(" , p.stamp ");
			sb.Append(" , p.uploaded ");
			sb.Append(" , p.delete_date ");
			sb.Append(" , p.userid as [located_at] ");
			sb.Append(" from sysadm.breed_req b ");
			sb.Append(" left outer join sysadm.picture_it p ");
			sb.Append(" on p.image_id in ('t' + convert(varchar(20), breed_req_id) ");
			sb.Append(" , 'x' + convert(varchar(20), breed_req_id) ");
			sb.Append(" , convert(varchar(20), breed_req_id)) ");
			sb.Append(" and p.image_resolution <> 'THUMB' ");
			sb.Append(" and p.image_seq = (select max(image_seq) ");
			sb.Append(" from sysadm.picture_it ");
			sb.Append(" where image_id in ('t' + convert(varchar(20), breed_req_id) ");
			sb.Append(" , 'x' + convert(varchar(20), breed_req_id) ");
			sb.Append(" , convert(varchar(20), breed_req_id)) ");
			sb.Append(" and image_resolution<> 'THUMB' ");
			sb.Append(" ) ");
			sb.Append(" where ");
			sb.Append(queryParam);
			sb.Append(" order by breed_req_id desc ");

			var sql = sb.ToString();
			var connection = new SqlConnection(animal.Database.Connection.ConnectionString);
			using (var command = new SqlCommand(sql, connection))
			{
				foreach (var item in addWithValue)
				{
					command.Parameters.AddWithValue(item.lookupString, item.lookupObject);
				}
				connection.Open();

				using (var reader = command.ExecuteReader())
				{
					pictureTable.Load(reader);
				}
			}
			return loadImageInformation(pictureTable);
		}

		internal ImageInformation getIndividualImageByID(string iD, int sequence)
		{
			ConnectionTools.ChangeDatabase(animal, userId: UserName, password: Password);

			var breedReqId = iD.Substring(1);

			/// Get the list of images
			var pictureTable = new DataTable();
			var sb = new StringBuilder();
			sb.Append(" select ");
			sb.Append(" p.image_id, ");
			sb.Append(" p.image_seq, ");
			sb.Append(" p.image_data, ");
			sb.Append(" p.image_resolution, ");
			sb.Append(" p.image_type, ");
			sb.Append(" p.userid, ");
			sb.Append(" p.[location], ");
			sb.Append(" p.stamp, ");
			sb.Append(" p.uploaded, ");
			sb.Append(" p.delete_date, ");
			sb.Append(" p.userid as [located_at], ");
			sb.Append(" b.email_address, ");
			sb.Append(" b.interest_type, ");
			sb.Append(" b.first_name, ");
			sb.Append(" b.last_name, ");
			sb.Append(" b.req_stat, ");
			sb.Append(" b.breed_req_id, ");
			sb.Append(" b.proper_name ");
			sb.Append(" from ");
			sb.Append(" sysadm.breed_req b join sysadm.picture_it p  ");
			sb.Append(" on 'T'+cast(b.breed_req_id as varchar(10)) = p.image_id ");
			sb.Append(" WHERE p.userid = 'PUBLIC' and p.location = 'PetReq' and b.req_stat <> 'CANCELLED'  ");
			sb.Append($" and b.breed_req_id =  @{nameof(breedReqId)} ");
			sb.Append($" and p.image_seq =  @{nameof(sequence)} ");
			//sb.Append(" and p.image_id = 'T' + cast(b.breed_req_id as varchar(10))  ");
			//sb.Append(" WHERE p.userid = 'PUBLIC' and p.location = 'PetReq' and p.image_id like 'T%' ");
			sb.Append(" AND image_resolution = 'DETAIL' ORDER BY image_seq, image_id desc ");

			var sql = sb.ToString();
			var connection = new SqlConnection(animal.Database.Connection.ConnectionString);
			try
			{
				using (var command = new SqlCommand(sql, connection))
				{
					command.Parameters.AddWithValue($"{nameof(breedReqId)}", breedReqId);
					command.Parameters.AddWithValue($"{nameof(sequence)}", sequence);
					connection.Open();
					using (var reader = command.ExecuteReader())
					{
						pictureTable.Load(reader);
					}
				}
			}
			catch (Exception e)
			{
				Console.Error.WriteLine(e);
			}

			return new ImageInformation
			{
				Approved = true,
				ImageNew = false,
				ImageId = pictureTable.Rows[0]["image_id"].ToString(),
				ImageSequence = Convert.ToInt16(pictureTable.Rows[0]["image_seq"]),
				ImageData = pictureTable.Rows[0]["image_data"].ToString() == null ? null : string.Format("data:image/" + pictureTable.Rows[0]["image_type"].ToString() + ";base64,{0}", Convert.ToBase64String((byte[])pictureTable.Rows[0]["image_data"])),
				ImageResolution = pictureTable.Rows[0]["image_resolution"].ToString(),
				ImageType = pictureTable.Rows[0]["image_type"].ToString(),
				UserId = pictureTable.Rows[0]["userid"].ToString(),
				Location = pictureTable.Rows[0]["location"].ToString(),
				Stamp = Convert.ToDateTime(pictureTable.Rows[0]["stamp"]),
				//Uploaded = $"{row["uploaded"]}"?.ToString() == null ? (DateTime?)null : Convert.ToDateTime($"{row["uploaded"]}"),
				//DeleteDate = $"{row["delete_date"]}"?.ToString() == null ? (DateTime?)null : Convert.ToDateTime($"{row["delete_date"]}"),
				PersonInfo = new Person
				{
					FirstName = pictureTable.Rows[0]["first_name"].ToString(),
					LastName = pictureTable.Rows[0]["last_name"].ToString(),
					EmailAddress = pictureTable.Rows[0]["email_address"].ToString(),
				},
				BreedRequestInfo = new BreedRequest
				{
					BreedRequestId = pictureTable.Rows[0]["breed_req_id"].ToString(),
					RequestStatus = pictureTable.Rows[0]["req_stat"].ToString(),
					ProperName = pictureTable.Rows[0]["proper_name"].ToString(),
					InterestType = pictureTable.Rows[0]["interest_type"].ToString()
				}
			};
		}

		internal void approveAllImages(List<ImageInformation> imageList)
		{
			ConnectionTools.ChangeDatabase(animal, userId: UserName, password: Password);

			foreach (var image in imageList)
			{
				var sb = new StringBuilder();
				var numaricId = "";
				var numaricSubstring = "";
				if (string.IsNullOrEmpty(image.ImageId))
				{
					numaricId = image.BreedRequestInfo.BreedRequestId;
				}
				else
				{
					numaricSubstring = image.ImageId.Substring(0, 1);
				}
				switch (numaricSubstring)
				{
					case "X":
					case "T":
						numaricId = image.ImageId.Substring(1);
						break;
					default:
						if (string.IsNullOrEmpty(image.ImageId))
						{
							numaricId = image.BreedRequestInfo.BreedRequestId;
						}
						else
						{
							numaricId = image.ImageId;
						}

						break;
				}

				if (image.Approved == true)
				{
					if (image.ImageNew == true)
					{
						var newImageString = image.ImageData.Substring(image.ImageData.IndexOf(',') + 1);
						var convertedImageByte = Convert.FromBase64String(newImageString);
						var newImageOutput = convertedImageByte;

						sb.Append($" insert into sysadm.picture_it ");
						sb.Append($" ( image_id ");
						sb.Append($" ,image_seq  ");
						sb.Append($" ,image_data  ");
						sb.Append($" ,image_resolution  ");
						sb.Append($" ,image_type  ");
						sb.Append($" ,userid  ");
						sb.Append($" ,[location]  ");
						sb.Append($" ,stamp  ");
						sb.Append($" ) values ( ");
						sb.Append($" @{nameof(numaricId)} ");
						sb.Append($" ,@{nameof(image.ImageSequence)} ");
						sb.Append($" ,@{nameof(newImageOutput)} ");
						sb.Append($"  ,'detail' ");
						sb.Append($" ,'JPG' ");
						sb.Append($" ,'PUBLIC' ");
						sb.Append($" ,'PetReq' ");
						sb.Append($" ,getdate() ) ");
						try
						{
							animal.Database.ExecuteSqlCommand(sb.ToString(),
									new SqlParameter($"{nameof(numaricId)}", numaricId),
									new SqlParameter($"{nameof(image.ImageSequence)}", image.ImageSequence),
									new SqlParameter($"{nameof(newImageOutput)}", newImageOutput)
									);
						}
						catch (Exception e)
						{
							var exception = e;
						}
					}
					else if (image.ImageChanged == true && !String.IsNullOrEmpty(image.ImageData))
					{

						var newImageString = image.ImageData.Substring(image.ImageData.IndexOf(',') + 1);
						var convertedImageByte = Convert.FromBase64String(newImageString);

						var newImageOutput = convertedImageByte;

						sb.Append($" update sysadm.picture_it ");
						sb.Append($" set image_id = @{nameof(numaricId)} ");
						sb.Append($" ,image_data = @{nameof(newImageOutput)} ");
						sb.Append($" ,stamp = getdate() ");
						sb.Append($" where image_id = @{nameof(image.ImageId)}");
						sb.Append($" and image_seq = @{nameof(image.ImageSequence)}");
						sb.Append($" and userid = @{nameof(image.UserId)}");
						try
						{
							animal.Database.ExecuteSqlCommand(sb.ToString(),
									new SqlParameter($"{nameof(numaricId)}", numaricId),
									new SqlParameter($"{nameof(newImageOutput)}", newImageOutput),
									new SqlParameter($"{nameof(image.ImageId)}", image.ImageId),
									new SqlParameter($"{nameof(image.ImageSequence)}", image.ImageSequence),
									new SqlParameter($"{nameof(image.UserId)}", image.UserId)
									);
						}
						catch (Exception e)
						{
							var exception = e;
						}
					}
					else if (numaricSubstring == "T") //These are temp images that are not changed 
					{
						sb.Append($" update sysadm.picture_it ");
						sb.Append($" set image_id = @{nameof(numaricId)} ");
						sb.Append($" ,stamp = getdate() ");
						sb.Append($" where image_id = @{nameof(image.ImageId)}");
						sb.Append($" and image_seq = @{nameof(image.ImageSequence)}");
						sb.Append($" and userid = @{nameof(image.UserId)}");
						try
						{
							animal.Database.ExecuteSqlCommand(sb.ToString(),
									new SqlParameter($"{nameof(numaricId)}", numaricId),
							new SqlParameter($"{nameof(image.ImageId)}", image.ImageId),
								new SqlParameter($"{nameof(image.ImageSequence)}", image.ImageSequence),
								new SqlParameter($"{nameof(image.UserId)}", image.UserId)
									);
						}
						catch (Exception e)
						{
							var exception = e;
						}
					}

				}
			}
		}

		internal void updateBreedRequestInformation(List<ImageInformation> list)
		{
			if (list != null && list.Count > 0)
			{
				// first get breed info
				var breedInfoList = new List<UtilityModel.BreedInfo>(getBreedInfo());
				foreach (var item in list)
				{
					// Set breed group and proper name
					item.BreedRequestInfo.ProperName = breedInfoList.Where(b => b.breedShortName == item.BreedRequestInfo.BreedName).Select(b => b.breedProperName).FirstOrDefault();
					item.BreedRequestInfo.BreedGroup = breedInfoList.Where(b => b.breedShortName == item.BreedRequestInfo.BreedName).Select(b => b.breedGroup).FirstOrDefault();

					var modifiedDate = DateTime.Now;

					// Update each record
					var sb = new StringBuilder();
					sb.Append($" update sysadm.[breed_req] ");
					sb.Append($" set req_stat = @{nameof(item.BreedRequestInfo.RequestStatus)} ");
					sb.Append($" ,[shelter_list] = @{nameof(item.BreedRequestInfo.ShelterList)} ");
					sb.Append($" ,[breed_name] = @{nameof(item.BreedRequestInfo.BreedName)} ");
					sb.Append($" ,[Interest_Type] = @{nameof(item.BreedRequestInfo.InterestType)} ");
					sb.Append($" ,[First_Name] = @{nameof(item.PersonInfo.FirstName)} ");
					sb.Append($" ,[Last_Name] = @{nameof(item.PersonInfo.LastName)} ");
					sb.Append($" ,[area_code] = @{nameof(item.PersonInfo.AreaCode)} ");
					sb.Append($" ,[phone_Number] = @{nameof(item.PersonInfo.PhoneNumber)} ");
					sb.Append($" ,[address] = @{nameof(item.PersonInfo.PatronAddress)} ");
					sb.Append($" ,[city] = @{nameof(item.PersonInfo.City)} ");
					sb.Append($" ,[state] = @{nameof(item.PersonInfo.State)} ");
					sb.Append($" ,[zip_code] = @{nameof(item.PersonInfo.ZipCode)} ");
					sb.Append($" ,[email_format] = @{nameof(item.PersonInfo.EmailFormat)} ");
					sb.Append($" ,[proper_name] = @{nameof(item.BreedRequestInfo.ProperName)} ");
					sb.Append($" ,[animal_type] = @{nameof(item.BreedRequestInfo.AnimalType)} ");
					sb.Append($" ,[match_gender] = @{nameof(item.BreedRequestInfo.AnimalGender)} ");
					sb.Append($" ,[match_breed_group] = @{nameof(item.BreedRequestInfo.BreedGroup)} ");
					sb.Append($" ,[match_age] = @{nameof(item.BreedRequestInfo.Age)} ");
					sb.Append($" ,[match_size] = @{nameof(item.BreedRequestInfo.Size)} ");
					sb.Append($" ,[looks_like] = @{nameof(item.BreedRequestInfo.SimilarAnimals)} ");
					sb.Append($" ,[match_color_group] = @{nameof(item.BreedRequestInfo.Color)} ");
					sb.Append($" ,[location_lostfound] = @{nameof(item.PersonInfo.LostFoundAddress)} ");
					sb.Append($" ,[animal_desc] = @{nameof(item.BreedRequestInfo.AnimalDescription)} ");
					sb.Append($" ,[no_pit_bull] = @{nameof(item.BreedRequestInfo.AllowPitBull)} ");
					sb.Append($" ,[event_date] = @{nameof(item.BreedRequestInfo.EventDate)} ");
					sb.Append($" ,[lost_pet_name] = @{nameof(item.BreedRequestInfo.PetName)} ");
					//sb.Append($" ,[lost_pet_owner_addr] = @{nameof(item.PersonInfo.PatronAddress)} ");
					sb.Append($" ,[secondary_color] = @{nameof(item.BreedRequestInfo.Color)} ");
					sb.Append($" ,[coat] = @{nameof(item.BreedRequestInfo.Coat)} ");
					sb.Append($" ,[tail] = @{nameof(item.BreedRequestInfo.Tail)} ");
					sb.Append($" ,[ears] = @{nameof(item.BreedRequestInfo.Ears)} ");
					sb.Append($" ,[nose] = @{nameof(item.BreedRequestInfo.Nose)} ");
					sb.Append($" ,[collar_type] = @{nameof(item.BreedRequestInfo.CollarType)} ");
					sb.Append($" ,[collar_color] = @{nameof(item.BreedRequestInfo.CollarColor)} ");
					sb.Append($" ,[tattoo] = @{nameof(item.BreedRequestInfo.Tattoo)} ");
					sb.Append($" ,[spec_event] = @{nameof(item.BreedRequestInfo.SpecialEvent)} ");
					sb.Append($" ,[microchip] = @{nameof(item.BreedRequestInfo.Microchip)} ");
					sb.Append($" ,[modified_date] = @{nameof(modifiedDate)} ");
					sb.Append($" where breed_req_id = @{nameof(item.BreedRequestInfo.BreedRequestId)}");
					try
					{
						animal.Database.ExecuteSqlCommand(sb.ToString(),
							new SqlParameter($"{nameof(item.BreedRequestInfo.RequestStatus)}", item.BreedRequestInfo.RequestStatus),
							new SqlParameter($"{nameof(item.BreedRequestInfo.ShelterList)}", String.IsNullOrWhiteSpace(item.BreedRequestInfo.ShelterList) ? (object)DBNull.Value : item.BreedRequestInfo.ShelterList),
							new SqlParameter($"{nameof(item.BreedRequestInfo.BreedName)}", item.BreedRequestInfo.BreedName),
							new SqlParameter($"{nameof(item.BreedRequestInfo.InterestType)}", String.IsNullOrWhiteSpace(item.BreedRequestInfo.InterestType) ? (object)DBNull.Value : item.BreedRequestInfo.InterestType),

							new SqlParameter($"{nameof(item.PersonInfo.FirstName)}", String.IsNullOrWhiteSpace(item.PersonInfo.FirstName) ? (object)DBNull.Value : item.PersonInfo.FirstName),
							new SqlParameter($"{nameof(item.PersonInfo.LastName)}", String.IsNullOrWhiteSpace(item.PersonInfo.LastName) ? (object)DBNull.Value : item.PersonInfo.LastName),
							new SqlParameter($"{nameof(item.PersonInfo.AreaCode)}", String.IsNullOrWhiteSpace(item.PersonInfo.AreaCode) ? (object)DBNull.Value : item.PersonInfo.AreaCode),
							new SqlParameter($"{nameof(item.PersonInfo.PhoneNumber)}", String.IsNullOrWhiteSpace(item.PersonInfo.PhoneNumber) ? (object)DBNull.Value : item.PersonInfo.PhoneNumber),
							new SqlParameter($"{nameof(item.PersonInfo.PatronAddress)}", String.IsNullOrWhiteSpace(item.PersonInfo.PatronAddress) ? (object)DBNull.Value : item.PersonInfo.PatronAddress),
							new SqlParameter($"{nameof(item.PersonInfo.City)}", String.IsNullOrWhiteSpace(item.PersonInfo.City) ? (object)DBNull.Value : item.PersonInfo.City),
							new SqlParameter($"{nameof(item.PersonInfo.State)}", String.IsNullOrWhiteSpace(item.PersonInfo.State) ? (object)DBNull.Value : item.PersonInfo.State),
							new SqlParameter($"{nameof(item.PersonInfo.ZipCode)}", String.IsNullOrWhiteSpace(item.PersonInfo.ZipCode) ? (object)DBNull.Value : item.PersonInfo.ZipCode),
							new SqlParameter($"{nameof(item.PersonInfo.EmailFormat)}", String.IsNullOrWhiteSpace(item.PersonInfo.EmailFormat) ? (object)DBNull.Value : item.PersonInfo.EmailFormat),

							new SqlParameter($"{nameof(item.BreedRequestInfo.ProperName)}", String.IsNullOrWhiteSpace(item.BreedRequestInfo.ProperName) ? (object)DBNull.Value : item.BreedRequestInfo.ProperName),
							new SqlParameter($"{nameof(item.BreedRequestInfo.AnimalType)}", String.IsNullOrWhiteSpace(item.BreedRequestInfo.AnimalType) ? (object)DBNull.Value : item.BreedRequestInfo.AnimalType),
							new SqlParameter($"{nameof(item.BreedRequestInfo.AnimalGender)}", String.IsNullOrWhiteSpace(item.BreedRequestInfo.AnimalGender) ? (object)DBNull.Value : item.BreedRequestInfo.AnimalGender),
							new SqlParameter($"{nameof(item.BreedRequestInfo.BreedGroup)}", String.IsNullOrWhiteSpace(item.BreedRequestInfo.BreedGroup) ? (object)DBNull.Value : item.BreedRequestInfo.BreedGroup),
							new SqlParameter($"{nameof(item.BreedRequestInfo.Age)}", String.IsNullOrWhiteSpace(item.BreedRequestInfo.Age) ? (object)DBNull.Value : item.BreedRequestInfo.Age),
							new SqlParameter($"{nameof(item.BreedRequestInfo.Size)}", String.IsNullOrWhiteSpace(item.BreedRequestInfo.Size) ? (object)DBNull.Value : item.BreedRequestInfo.Size),
							new SqlParameter($"{nameof(item.BreedRequestInfo.SimilarAnimals)}", String.IsNullOrWhiteSpace(item.BreedRequestInfo.SimilarAnimals) ? (object)DBNull.Value : item.BreedRequestInfo.SimilarAnimals),
							new SqlParameter($"{nameof(item.BreedRequestInfo.Color)}", String.IsNullOrWhiteSpace(item.BreedRequestInfo.Color) ? (object)DBNull.Value : item.BreedRequestInfo.Color),
							//new SqlParameter($"{nameof(item.BreedRequestInfo.LocationLostFound)}", String.IsNullOrWhiteSpace(item.BreedRequestInfo.LocationLostFound) ? (object)DBNull.Value : item.BreedRequestInfo.LocationLostFound),
							new SqlParameter($"{nameof(item.BreedRequestInfo.AnimalDescription)}", String.IsNullOrWhiteSpace(item.BreedRequestInfo.AnimalDescription) ? (object)DBNull.Value : item.BreedRequestInfo.AnimalDescription),
							new SqlParameter($"{nameof(item.BreedRequestInfo.AllowPitBull)}", String.IsNullOrWhiteSpace(item.BreedRequestInfo.AllowPitBull) ? (object)DBNull.Value : item.BreedRequestInfo.AllowPitBull),
							new SqlParameter($"{nameof(item.BreedRequestInfo.EventDate)}", String.IsNullOrWhiteSpace(item.BreedRequestInfo.EventDate) ? (object)DBNull.Value : item.BreedRequestInfo.EventDate),
							new SqlParameter($"{nameof(item.BreedRequestInfo.PetName)}", String.IsNullOrWhiteSpace(item.BreedRequestInfo.PetName) ? (object)DBNull.Value : item.BreedRequestInfo.PetName),
							new SqlParameter($"{nameof(item.PersonInfo.LostFoundAddress)}", String.IsNullOrWhiteSpace(item.PersonInfo.LostFoundAddress) ? (object)DBNull.Value : item.PersonInfo.LostFoundAddress),
							//new SqlParameter($"{nameof(item.BreedRequestInfo.Color)}", String.IsNullOrWhiteSpace(item.BreedRequestInfo.Color) ? (object)DBNull.Value : item.BreedRequestInfo.Color),
							new SqlParameter($"{nameof(item.BreedRequestInfo.Coat)}", String.IsNullOrWhiteSpace(item.BreedRequestInfo.Coat) ? (object)DBNull.Value : item.BreedRequestInfo.Coat),
							new SqlParameter($"{nameof(item.BreedRequestInfo.Tail)}", String.IsNullOrWhiteSpace(item.BreedRequestInfo.Tail) ? (object)DBNull.Value : item.BreedRequestInfo.Tail),
							new SqlParameter($"{nameof(item.BreedRequestInfo.Ears)}", String.IsNullOrWhiteSpace(item.BreedRequestInfo.Ears) ? (object)DBNull.Value : item.BreedRequestInfo.Ears),
							new SqlParameter($"{nameof(item.BreedRequestInfo.Nose)}", String.IsNullOrWhiteSpace(item.BreedRequestInfo.Nose) ? (object)DBNull.Value : item.BreedRequestInfo.Nose),
							new SqlParameter($"{nameof(item.BreedRequestInfo.CollarType)}", String.IsNullOrWhiteSpace(item.BreedRequestInfo.CollarType) ? (object)DBNull.Value : item.BreedRequestInfo.CollarType),
							new SqlParameter($"{nameof(item.BreedRequestInfo.CollarColor)}", String.IsNullOrWhiteSpace(item.BreedRequestInfo.CollarColor) ? (object)DBNull.Value : item.BreedRequestInfo.CollarColor),
							new SqlParameter($"{nameof(item.BreedRequestInfo.Tattoo)}", String.IsNullOrWhiteSpace(item.BreedRequestInfo.Tattoo) ? (object)DBNull.Value : item.BreedRequestInfo.Tattoo),
							new SqlParameter($"{nameof(item.BreedRequestInfo.SpecialEvent)}", String.IsNullOrWhiteSpace(item.BreedRequestInfo.SpecialEvent) ? (object)DBNull.Value : item.BreedRequestInfo.SpecialEvent),
							new SqlParameter($"{nameof(item.BreedRequestInfo.Microchip)}", String.IsNullOrWhiteSpace(item.BreedRequestInfo.Microchip) ? (object)DBNull.Value : item.BreedRequestInfo.Microchip),
							new SqlParameter($"{nameof(modifiedDate)}", modifiedDate),
							new SqlParameter($"{nameof(item.BreedRequestInfo.BreedRequestId)}", item.BreedRequestInfo.BreedRequestId)
						);
					}
					catch (Exception e)
					{
						var exception = e;
					}

				}

			}
		}

		internal List<UtilityModel.StateList> getStatesList()
		{
			ConnectionTools.ChangeDatabase(animal, userId: UserName, password: Password);

			var sb = new StringBuilder();
			sb.Append(" select * from sysadm.utilityStates ");

			var sql = sb.ToString();
			DataTable stateTable = new DataTable();

			var connection = new SqlConnection(animal.Database.Connection.ConnectionString);
			try
			{
				using (var command = new SqlCommand(sql, connection))
				{
					connection.Open();
					using (var reader = command.ExecuteReader())
					{
						stateTable.Load(reader);
					}
				}
			}
			catch (Exception e)
			{
				Console.Error.WriteLine(e);
			}

			var stateList = new List<UtilityModel.StateList>();
			foreach (DataRow row in stateTable.Rows)
			{
				stateList.Add(new UtilityModel.StateList
				{
					stateKey = $"{row["stateAbbr"]}"?.ToString(),
					stateText = $"{row["stateText"]}"?.ToString(),
				});
			}
			return stateList;
		}

		internal List<UtilityModel.BreedInfo> getBreedInfo()
		{
			ConnectionTools.ChangeDatabase(animal, userId: UserName, password: Password);

			var sb = new StringBuilder();
			sb.Append(" select * from sysadm.breeds ");

			var sql = sb.ToString();
			DataTable dTable = new DataTable();

			var connection = new SqlConnection(animal.Database.Connection.ConnectionString);
			try
			{
				using (var command = new SqlCommand(sql, connection))
				{
					connection.Open();
					using (var reader = command.ExecuteReader())
					{
						dTable.Load(reader);
					}
				}
			}
			catch (Exception e)
			{
				Console.Error.WriteLine(e);
			}

			var list = new List<UtilityModel.BreedInfo>();
			list.Add(new UtilityModel.BreedInfo
			{
				breedKey = 0,
				breedShortName = "",
				breedProperName = "",
				breedGroup = "",
				animalType = "",
				species = "",
			});
			foreach (DataRow row in dTable.Rows)
			{
				list.Add(new UtilityModel.BreedInfo
				{
					breedKey = Convert.ToInt32($"{row["idn"]}"),
					breedShortName = $"{row["shortName"]}"?.ToString(),
					breedProperName = $"{row["properName"]}"?.ToString(),
					breedGroup = $"{row["breedGroup"]}"?.ToString(),
					animalType = $"{row["animalType"]}"?.ToString(),
					species = $"{row["species"]}"?.ToString(),
				});
			}
			return list;



		}

		internal List<ImageInformation> loadImageInformation(DataTable pictureTable)
		{
			short noValue = 0;
			var pictureList = new List<ImageInformation>();
			foreach (DataRow row in pictureTable.Rows)
			{
				pictureList.Add(new ImageInformation
				{
					//ImageLink = longLink,
					Approved = true,
					ImageChanged = false,
					ImageId = $"{row["image_id"]}"?.ToString(),
					ImageSequence = string.IsNullOrEmpty($"{row["image_seq"]}"?.ToString()) ? noValue : Convert.ToInt16($"{row["image_seq"]}"),
					ImageData = string.IsNullOrEmpty($"{row["image_data"]}"?.ToString()) ? null : string.Format("data:image/" + $"{row["image_type"]}"?.ToString() + ";base64,{0}", Convert.ToBase64String((byte[])row["image_data"])),
					ImageResolution = $"{row["image_resolution"]}"?.ToString(),
					ImageType = $"{row["image_type"]}"?.ToString(),
					UserId = $"{row["userid"]}"?.ToString(),
					Location = $"{row["location"]}"?.ToString(),
					Stamp = string.IsNullOrEmpty($"{row["image_data"]}"?.ToString()) ? DateTime.Now : Convert.ToDateTime($"{row["stamp"]}"),
					//Uploaded = $"{row["uploaded"]}"?.ToString() == null ? (DateTime?)null : Convert.ToDateTime($"{row["uploaded"]}"),
					//DeleteDate = $"{row["delete_date"]}"?.ToString() == null ? (DateTime?)null : Convert.ToDateTime($"{row["delete_date"]}"),
					PersonInfo = new Person
					{
						FirstName = $"{row["first_name"]}"?.ToString(),
						LastName = $"{row["last_name"]}"?.ToString(),
						AreaCode = $"{row["area_code"]}"?.ToString(),
						PhoneNumber = $"{row["phone_Number"]}"?.ToString(),
						LostFoundAddress = $"{row["location_lostfound"]}"?.ToString(),
						PatronAddress = $"{row["address"]}"?.ToString(),
						City = $"{row["city"]}"?.ToString(),
						State = $"{row["state"]}"?.ToString(),
						ZipCode = $"{row["zip_code"]}"?.ToString(),
						EmailAddress = $"{row["email_address"]}"?.ToString(),
						EmailFormat = $"{row["email_format"]}"?.ToString(),
						//ZipCode = $"{row["zip_code"]}"?.ToString()
					},
					BreedRequestInfo = new BreedRequest
					{
						BreedRequestId = $"{row["breed_req_id"]}"?.ToString(),
						RequestStatus = $"{row["req_stat"]}"?.ToString(),
						ShelterList = $"{row["shelter_list"]}"?.ToString(),
						BreedName = $"{row["breed_name"]}"?.ToString(),
						ProperName = $"{row["proper_name"]}"?.ToString(),
						InterestType = $"{row["interest_type"]}"?.ToString(),
						AnimalType = $"{row["animal_type"]}"?.ToString(),
						AnimalGender = $"{row["match_gender"]}"?.ToString(),
						BreedGroup = $"{row["match_breed_group"]}"?.ToString(),
						Age = $"{row["match_age"]}"?.ToString(),
						Size = $"{row["match_size"]}"?.ToString(),
						SimilarAnimals = $"{row["looks_like"]}"?.ToString(),
						Color = $"{row["match_color_group"]}"?.ToString(),
						//LocationLostFound = $"{row["location_lostfound"]}"?.ToString(),
						AnimalDescription = $"{row["animal_desc"]}"?.ToString(),
						AllowPitBull = $"{row["no_pit_bull"]}"?.ToString(),
						EventDate = $"{row["event_date"]}"?.ToString(),
						PetName = $"{row["lost_pet_name"]}"?.ToString(),
						//LostPetOwnerAddress = $"{row["lost_pet_owner_addr"]}"?.ToString(),
						Coat = $"{row["coat"]}"?.ToString(),
						Tail = $"{row["tail"]}"?.ToString(),
						Ears = $"{row["ears"]}"?.ToString(),
						Nose = $"{row["nose"]}"?.ToString(),
						CollarType = $"{row["collar_type"]}"?.ToString(),
						CollarColor = $"{row["collar_color"]}"?.ToString(),
						Tattoo = $"{row["tattoo"]}"?.ToString(),
						SpecialEvent = $"{row["spec_event"]}"?.ToString(),
						Microchip = $"{row["microchip"]}"?.ToString()
					}
				});
			}
			return pictureList;
		}



		#endregion

		#region PH3Configs

		internal DataTable CheckURLNameAvailability(string URLName)
		{
			//ConnectionTools.ChangeDatabase(animal, userId: UserName, password: Password);

			var sb = new StringBuilder();
			sb.Append(" select * from sysadm.[PH3ClientResultsView] ");
			sb.Append($" where urlName = @{nameof(URLName)} ");

			var resultsTable = new DataTable();
			using (var connection = new SqlConnection(animal.Database.Connection.ConnectionString))
			using (var command = new SqlCommand(sb.ToString(), connection))
			{
				command.Parameters.AddWithValue($"{nameof(URLName)}", URLName);
				connection.Open();
				using (var reader = command.ExecuteReader())
				{
					resultsTable.Load(reader);
				}
			}
			return resultsTable;
		}

		internal string getClientInformation(string PH3clientId)
		{
			var sb = new StringBuilder();
			sb.Append(" Select client_name ");
			sb.Append(" from sysadm.client ");
			sb.Append($" where client_id = @{nameof(PH3clientId)} ");

			var clientName = "";
			try
			{
				clientName = animal.Database.SqlQuery<string>(sb.ToString(),
						new SqlParameter($"{nameof(PH3clientId)}", PH3clientId.ToUpper())
						).FirstOrDefault();
			}
			catch (Exception e) {
			
			}
			return clientName;
		}


		internal int CreateClientResultsViewRow(string type, string URLName, string shelter)
		{
			//ConnectionTools.ChangeDatabase(animal, userId: UserName, password: Password);
			var clientName = getClientInformation(shelter);

			var resultsView = "";
			if(type == "adopt")
      {
				resultsView = "v_PH3_Adopt";
      }else if(type == "lost")
      {
				resultsView = "v_PH3_Lost";
      }else if(type == "found")
      {
				resultsView = "v_PH3_Found";
      }

			var sb = new StringBuilder();
			sb.Append(" insert into sysadm.[PH3ClientResultsView] ");
			sb.Append($"( clientName, clientId, resultsName, resultsView, urlName ) values ( ");
			sb.Append($"@{nameof(clientName)}, ");
			sb.Append($"@{nameof(shelter)}, ");
			sb.Append($"@{nameof(type)}, ");
			sb.Append($"@{nameof(resultsView)}, ");
			sb.Append($"@{nameof(URLName)} ");
			sb.Append($"); select scope_identity()");

			decimal identifierRowId = 0;
			try
			{
				identifierRowId = animal.Database.SqlQuery<decimal>(sb.ToString(),
						new SqlParameter($"{nameof(clientName)}", clientName),
						new SqlParameter($"{nameof(shelter)}", shelter.ToUpper()),
						new SqlParameter($"{nameof(type)}", type),
						new SqlParameter($"{nameof(resultsView)}", resultsView),
						new SqlParameter($"{nameof(URLName)}", URLName)
						).FirstOrDefault();
			}catch(Exception e)
      {

      }
			return (int)identifierRowId;
		}

		internal int InsertPH3Shelter(int idn, string shelter)
		{
			var sb = new StringBuilder();
			sb.Append(" insert into sysadm.[PH3ClientResultsViewShelterList] ");
			sb.Append($"( PH3ClientResultsViewidn, clientId ) values ( ");
			sb.Append($"@{nameof(idn)}, ");
			sb.Append($"@{nameof(shelter)} ");
			sb.Append($")");

			var identifierRowId = 0;
			try
			{
        animal.Database.ExecuteSqlCommand(sb.ToString(),
            new SqlParameter($"{nameof(idn)}", idn),
            new SqlParameter($"{nameof(shelter)}", shelter.ToUpper())
            );
      }
			catch (Exception e)
			{

			}
			return identifierRowId;
		}

		internal void InsertPH3Config(PH3ConfigModel.PH3ConfigRow row)
		{
			var sb = new StringBuilder();
			sb.Append(" insert into sysadm.[PH3ClientResultsViewConfiguration] ");
			sb.Append($"( PH3ClientResultsViewidn, identifier, display, customText ) values ( ");
			sb.Append($"@{nameof(row.PH3ClientResultsViewidn)}, ");
			sb.Append($"@{nameof(row.Identifier)}, ");
			sb.Append($"@{nameof(row.Display)}, ");
			sb.Append($"@{nameof(row.CustomText)} ");
			sb.Append($")");
			try
			{
        animal.Database.ExecuteSqlCommand(sb.ToString(),
            new SqlParameter($"{nameof(row.PH3ClientResultsViewidn)}", row.PH3ClientResultsViewidn),
            new SqlParameter($"{nameof(row.Identifier)}", row.Identifier),
						new SqlParameter($"{nameof(row.Display)}", row.Display),
						new SqlParameter($"{nameof(row.CustomText)}", String.IsNullOrWhiteSpace(row.CustomText) ? (object)DBNull.Value : row.CustomText)
						);
      }
			catch (Exception e)
			{
				throw e;
			}
		}

		internal DataTable getShelterDisplayPreferences(int shelterIDN)
		{
			var sql = new StringBuilder();
			sql.Append($" select * from sysadm.f_ph_client_view_config (" + shelterIDN + ")");

			var resultsTable = new DataTable();
			using (var connection = new SqlConnection(animal.Database.Connection.ConnectionString))
			using (var command = new SqlCommand(sql.ToString(), connection))
			{
				connection.Open();
				using (var reader = command.ExecuteReader())
				{
					resultsTable.Load(reader);
				}
			}
			return resultsTable;
		}




		#endregion

	}



	//}

	public static class ConnectionTools
	{
		// all params are optional
		public static void ChangeDatabase(
				this DbContext source,
				string initialCatalog = "",
				string dataSource = "",
				string userId = "",
				string password = "",
				bool integratedSecuity = false,
				string configConnectionStringName = "")
		/* this would be used if the
		*  connectionString name varied from 
		*  the base EF class name */
		{
			try
			{
				// use the const name if it's not null, otherwise
				// using the convention of connection string = EF contextname
				// grab the type name and we're done
				var configNameEf = string.IsNullOrEmpty(configConnectionStringName)
						? source.GetType().Name
						: configConnectionStringName;

				// add a reference to System.Configuration
				var entityCnxStringBuilder = new EntityConnectionStringBuilder
						(System.Configuration.ConfigurationManager
								.ConnectionStrings[configNameEf].ConnectionString);

				// init the sqlbuilder with the full EF connectionstring cargo
				var sqlCnxStringBuilder = new SqlConnectionStringBuilder
						(entityCnxStringBuilder.ProviderConnectionString);

				// only populate parameters with values if added
				if (!string.IsNullOrEmpty(initialCatalog))
					sqlCnxStringBuilder.InitialCatalog = initialCatalog;
				if (!string.IsNullOrEmpty(dataSource))
					sqlCnxStringBuilder.DataSource = dataSource;
				if (!string.IsNullOrEmpty(userId))
					sqlCnxStringBuilder.UserID = userId;
				if (!string.IsNullOrEmpty(password))
					sqlCnxStringBuilder.Password = password;

				// set the integrated security status
				sqlCnxStringBuilder.IntegratedSecurity = integratedSecuity;

        sqlCnxStringBuilder.ConnectTimeout = 240;

				// now flip the properties that were changed
				source.Database.Connection.ConnectionString
						= sqlCnxStringBuilder.ConnectionString;
			}
			catch (Exception ex)
			{
				// set log item if required
			}
		}
	}
}

